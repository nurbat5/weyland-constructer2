﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUIRender : MonoBehaviour
{
    [Header("Render Config")]
    public Transform contentRect;
    public GameObject categoryPrefab;
    public GameObject itemPrefab;

    [Header("ItemList")]
    public InventoryUIToolAsset[] inventoryUIToolAsset;
    private Dictionary<string, InventoryUIToolAsset> _listInventory = new Dictionary<string, InventoryUIToolAsset>();

    private void Start()
    {
        StartCoroutine(PreRender());
    }

    private IEnumerator PreRender()
    {
        for(int i = 0; i < inventoryUIToolAsset.Length; i++)
        {
            _listInventory[inventoryUIToolAsset[i].categoryName] = inventoryUIToolAsset[i];
        }
        yield return Render();
    }
    private IEnumerator Render()
    {
        foreach(string key in _listInventory.Keys)
        {
            Transform categoryObj = Instantiate(categoryPrefab, contentRect).transform;
            categoryObj.GetComponentInChildren<Text>().text = key;
            categoryObj = categoryObj.Find("Items");
            for(int t = 0; t < _listInventory.Values.Count; t++)
            {
                Transform itemObj = Instantiate(itemPrefab, categoryObj).transform;
                itemObj.Find("IconImage").GetComponent<Image>().sprite = _listInventory[key].icon;
            }
        }
        yield return new WaitForEndOfFrame();
    }
}
