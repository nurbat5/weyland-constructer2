﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Input;
using UnityEngine.UI;

[Serializable]
public class ToolbarUIRender
{
    public void UpdateKeys()
    {
        if(Keyboard.current.numpad0Key.wasReleasedThisFrame) elementActive = 0;
        else if(Keyboard.current.numpad1Key.wasReleasedThisFrame) elementActive = 1;
        else if(Keyboard.current.numpad2Key.wasReleasedThisFrame) elementActive = 2;
        else if(Keyboard.current.numpad3Key.wasReleasedThisFrame) elementActive = 3;
        else if(Keyboard.current.numpad4Key.wasReleasedThisFrame) elementActive = 4;
        else if(Keyboard.current.numpad5Key.wasReleasedThisFrame) elementActive = 5;
        else if(Keyboard.current.numpad6Key.wasReleasedThisFrame) elementActive = 6;
        else if(Keyboard.current.numpad7Key.wasReleasedThisFrame) elementActive = 7;
        else if(Keyboard.current.numpad8Key.wasReleasedThisFrame) elementActive = 8;
        else if(Keyboard.current.numpad9Key.wasReleasedThisFrame) elementActive = 9;
    }
    public int elementActive
    {
        get => _currentActive;
        set
        {
            for(int i = 0; i < _toolKeys.Length; i++) _toolKeys[i].activeElement.SetActive(i == (_currentActive = value));
        }
    }
    public InventoryUIToolAsset GetAsset() => GetAsset(_currentActive);
    public InventoryUIToolAsset GetAsset(int index) => _toolKeys[Mathf.Clamp(index, 0, 9)].asset;
    public void SetAsset(InventoryUIToolAsset asset, int index)
    {
        index = Mathf.Clamp(index, 0, 10);
        _toolKeys[index].asset = asset;
        _toolKeys[index].image.gameObject.SetActive(asset != null); //if asset don't empty = true
    }

    public ToolbarUIRender(Transform transform)
    {
        _toolKeys = new toolbarItems[transform.Find("Content").childCount];
        for (int i = 0; i < _toolKeys.Length; i++)
        {
            toolbarItems item = new toolbarItems();
            item.element = transform.Find("Content").GetChild(i);
            item.activeElement = item.element.Find("Active").gameObject;
            item.image = item.element.GetComponentInChildren<Image>();
            if(i < _toolKeys.Length - 1) _toolKeys[i + 1] = item;
            else _toolKeys[0] = item;
        }
        elementActive = -1;
    }

    [Serializable]
    private struct toolbarItems
    {
        public Transform element;
        public GameObject activeElement;
        public Image image;
        public InventoryUIToolAsset asset;
    }
    private int _currentActive;
    private toolbarItems[] _toolKeys;
}
