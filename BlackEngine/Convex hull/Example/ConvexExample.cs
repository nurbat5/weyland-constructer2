﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshCollider))]
public class ConvexExample : MonoBehaviour
{
    [Range(0.1f, 1)]
    public double size = 1;
    public MeshTopology meshTopology = MeshTopology.Triangles;

	void Start () 
	{
		if(GetComponent<MeshCollider>().convex) Convex.GenerateConvex(GetComponent<MeshFilter>().mesh, meshTopology, size);
	}
}
