﻿namespace MIConvexHull
{
	static class Constants
	{
		/// <summary>
		/// A value used to determine if a vertex lies on a plane.
		/// </summary>
		public const double PlaneDistanceTolerance = 0.0000001;
	}
}
