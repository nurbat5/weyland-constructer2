﻿using UnityEngine;
using System.Collections.Generic;
using MIConvexHull;
using System.Threading.Tasks;

public class Convex
{
    // Use this for initialization
    public static Mesh GenerateConvex(Mesh mesh, MeshTopology meshTopology = MeshTopology.Triangles, double size = 1)
    {
        int NumberOfVertices = mesh.vertexCount;
        //FIX REALTIME CONVEX
        Vertex3[] vertices = new Vertex3[NumberOfVertices];

        for (var i = 0; i < NumberOfVertices; i++)
        {
            vertices[i] = new Vertex3(
                size * mesh.vertices[i].x,
                size * mesh.vertices[i].y,
                size * mesh.vertices[i].z);
        }

        //mesh.bounds = new Bounds(Vector3.zero, new Vector3((float)size,(float)size,(float)size));

        float now = Time.realtimeSinceStartup;//DEBUG #1
        ConvexHull<Vertex3, Face3> convexHull = ConvexHull.Create<Vertex3, Face3>(vertices);

        float interval = Time.realtimeSinceStartup - now;//DEBUG #2

        List<Vertex3> convexHullVertices = new List<Vertex3>(convexHull.Points);
        List<Face3> convexHullFaces = new List<Face3>(convexHull.Faces);
        List<int> convexHullIndices = new List<int>();//Tringles

        foreach (Face3 f in convexHullFaces)
        {
            convexHullIndices.Add(convexHullVertices.IndexOf(f.Vertices[0]));
            convexHullIndices.Add(convexHullVertices.IndexOf(f.Vertices[1]));
            convexHullIndices.Add(convexHullVertices.IndexOf(f.Vertices[2]));
        }

        List<Vector3> meshVerts = new List<Vector3>();
        List<Vector3> meshNorms = new List<Vector3>();
        foreach (Vertex3 f in convexHullVertices)
        {
            meshVerts.Add(f.ToVector3());
            meshNorms.Add(f.ToVector3().normalized);
        }

        Vector2[] uvs = new Vector2[meshVerts.Count];
        for (var v = 0; v < meshVerts.Count; v++)
        {
            var vertex = meshVerts[v];
            var normal = meshNorms[v];
            // This gives a vector pointing up the roof:
            var vAxis = Vector3.Scale(normal, new Vector3(-1, 0, -1)).normalized;
            // This will make the u axis perpendicular to the v axis (ie. parallel to the roof edge)
            var uAxis = new Vector3(vAxis.y, 0, -vAxis.x);
            // I originally used vAxis here, but changed to position.y so you get more predticable alignment at edges.
            // Set eaveHeight to the y coordinate of the bottom edge of the roof.
            uvs[v] = new Vector2(Vector3.Dot(vertex, uAxis), vertex.y - 0);

            // You may need to scale the uv vector's x and y to get the aspect ratio you want.
            // The scale factor will vary with the roof's slope.
        }

        mesh.Clear();
        mesh.vertices = meshVerts.ToArray();
        mesh.triangles = convexHullIndices.ToArray();
        mesh.uv = uvs;//! Unwrapping.GeneratePerTriangleUV();
        mesh.bounds = new Bounds(Vector3.zero, new Vector3((float)1,(float)1,(float)1));
        mesh.normals = meshNorms.ToArray();
        mesh.SetIndices(convexHullIndices.ToArray(), meshTopology, 0);//Mode the view

        Debug.Log("Face:" + convexHullFaces.Count + " Triangles:" + convexHullIndices.Count);
        Debug.Log("Out of the " + NumberOfVertices + " vertices, there are " + convexHullVertices.Count + " verts on the convex hull.");
        Debug.Log("time = " + interval * 1000.0f + " ms");//DEBUG 3
        return mesh;
    }

    public static Mesh GenerateConvex(Mesh mesh, double size = 1)
    {
        int NumberOfVertices = mesh.vertexCount;
        //FIX REALTIME CONVEX
        Vertex3[] vertices = new Vertex3[NumberOfVertices];

        for (var i = 0; i < NumberOfVertices; i++)
        {
            vertices[i] = new Vertex3(
                size * mesh.vertices[i].x,
                size * mesh.vertices[i].y,
                size * mesh.vertices[i].z);
        }

        //mesh.bounds = new Bounds(Vector3.zero, new Vector3((float)size,(float)size,(float)size));

        float now = Time.realtimeSinceStartup;//DEBUG #1
        ConvexHull<Vertex3, Face3> convexHull = ConvexHull.Create<Vertex3, Face3>(vertices);

        float interval = Time.realtimeSinceStartup - now;//DEBUG #2

        List<Vertex3> convexHullVertices = new List<Vertex3>(convexHull.Points);
        List<Face3> convexHullFaces = new List<Face3>(convexHull.Faces);
        List<int> convexHullIndices = new List<int>();//Tringles

        foreach (Face3 f in convexHullFaces)
        {
            convexHullIndices.Add(convexHullVertices.IndexOf(f.Vertices[0]));
            convexHullIndices.Add(convexHullVertices.IndexOf(f.Vertices[1]));
            convexHullIndices.Add(convexHullVertices.IndexOf(f.Vertices[2]));
        }

        List<Vector3> meshVerts = new List<Vector3>();
        List<Vector3> meshNorms = new List<Vector3>();
        foreach (Vertex3 f in convexHullVertices)
        {
            meshVerts.Add(f.ToVector3());
            meshNorms.Add(f.ToVector3().normalized);
        }

        Vector2[] uvs = new Vector2[meshVerts.Count];
        for (var v = 0; v < meshVerts.Count; v++)
        {
            var vertex = meshVerts[v];
            var normal = meshNorms[v];
            // This gives a vector pointing up the roof:
            var vAxis = Vector3.Scale(normal, new Vector3(-1, 0, -1)).normalized;
            // This will make the u axis perpendicular to the v axis (ie. parallel to the roof edge)
            var uAxis = new Vector3(vAxis.y, 0, -vAxis.x);
            // I originally used vAxis here, but changed to position.y so you get more predticable alignment at edges.
            // Set eaveHeight to the y coordinate of the bottom edge of the roof.
            uvs[v] = new Vector2(Vector3.Dot(vertex, uAxis), vertex.y - 0);

            // You may need to scale the uv vector's x and y to get the aspect ratio you want.
            // The scale factor will vary with the roof's slope.
        }

        mesh.Clear();
        mesh.vertices = meshVerts.ToArray();
        mesh.triangles = convexHullIndices.ToArray();
        mesh.uv = uvs;//! Unwrapping.GeneratePerTriangleUV();
        mesh.bounds = new Bounds(Vector3.zero, new Vector3((float)1, (float)1, (float)1));
        mesh.normals = meshNorms.ToArray();
        //mesh.SetIndices(convexHullIndices.ToArray(), meshTopology, 0);//Mode the view

        Debug.Log("Face:" + convexHullFaces.Count + " Triangles:" + convexHullIndices.Count);
        Debug.Log("Out of the " + NumberOfVertices + " vertices, there are " + convexHullVertices.Count + " verts on the convex hull.");
        Debug.Log("time = " + interval * 1000.0f + " ms");//DEBUG 3
        return mesh;
    }

}