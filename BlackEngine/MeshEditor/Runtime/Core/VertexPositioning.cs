using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BlackEngine.MeshEditor.Runtime;

namespace BlackEngine.MeshEditor
{
    public static class VertexPositioning
    {
        static List<int> s_CoincidentVertices = new List<int>();

        /// <summary>
        /// Get a copy of a mesh positions array transformed into world coordinates.
        /// </summary>
        /// <param name="mesh">The source mesh.</param>
        /// <returns>An array containing all vertex positions in world space.</returns>
        public static Vector3[] VerticesInWorldSpace(this MeshInspector mesh)
        {
            if (mesh == null) throw new ArgumentNullException("mesh");

            int len = mesh.vertexCount;
            Vector3[] worldPoints = new Vector3[len];
            Vector3[] localPoints = mesh.verticesPositions;

            for (int i = 0; i < len; i++)
                worldPoints[i] = mesh.transform.TransformPoint(localPoints[i]);

            return worldPoints;
        }

        public static void TranslateVerticesInWorldSpace(this MeshInspector mesh, int[] indexes, Vector3 offset)
        {
            if (mesh == null) throw new ArgumentNullException("mesh");
            mesh.TranslateVerticesInWorldSpace(indexes, offset, 0f, false);
        }
        public static void TranslateVerticesInWorldSpace(this MeshInspector mesh, int[] indexes, Vector3 offset, float snapValue, bool snapAxisOnly)
        {
            if (mesh == null) throw new ArgumentNullException("mesh");
            int i = 0;
            mesh.GetCoincidentVertices(indexes, s_CoincidentVertices);

            Matrix4x4 w2l = mesh.transform.worldToLocalMatrix;

            Vector3 localOffset = w2l * offset;

            Vector3[] verts = mesh.verticesPositions;

            // Snaps to world grid
            if (Mathf.Abs(snapValue) > Mathf.Epsilon)
            {
                Matrix4x4 l2w = mesh.transform.localToWorldMatrix;
                var mask = snapAxisOnly ? new Vector3Mask(offset, MeshMath.handleEpsilon) : Vector3Mask.XYZ;

                for (i = 0; i < s_CoincidentVertices.Count; i++)
                {
                    var v = l2w.MultiplyPoint3x4(verts[s_CoincidentVertices[i]] + localOffset);
                    verts[s_CoincidentVertices[i]] = w2l.MultiplyPoint3x4(Snapping.SnapValue(v, ((Vector3)mask) * snapValue));
                }
            }
            else
            {
                for (i = 0; i < s_CoincidentVertices.Count; i++)
                    verts[s_CoincidentVertices[i]] += localOffset;
            }

            // don't bother calling a full ToMesh() here because we know for certain that the vertices and msh.vertices arrays are equal in length
            mesh.verticesPositions = verts;
            mesh.mesh.vertices = verts;
        }
        public static void TranslateVertices(this MeshInspector mesh, IEnumerable<int> indexes, Vector3 offset)
        {
            if (mesh == null) throw new ArgumentNullException("mesh");
            mesh.GetCoincidentVertices(indexes, s_CoincidentVertices);
            TranslateVerticesInternal(mesh, s_CoincidentVertices, offset);
        }
        public static void TranslateVertices(this MeshInspector mesh, IEnumerable<Edge> edges, Vector3 offset)
        {
            if (mesh == null) throw new ArgumentNullException("mesh");
            mesh.GetCoincidentVertices(edges, s_CoincidentVertices);
            TranslateVerticesInternal(mesh, s_CoincidentVertices, offset);
        }
        public static void TranslateVertices(this MeshInspector mesh, IEnumerable<Face> faces, Vector3 offset)
        {
            if (mesh == null)
                throw new ArgumentNullException("mesh");
            mesh.GetCoincidentVertices(faces, s_CoincidentVertices);
            TranslateVerticesInternal(mesh, s_CoincidentVertices, offset);
        }

        static void TranslateVerticesInternal(MeshInspector mesh, IEnumerable<int> indices, Vector3 offset)
        {
            Vector3[] verts = mesh.verticesPositions;

            for (int i = 0, c = s_CoincidentVertices.Count; i < c; i++)
                verts[s_CoincidentVertices[i]] += offset;

            // don't bother calling a full ToMesh() here because we know for certain that the vertices and msh.vertices arrays are equal in length
            mesh.mesh.vertices = verts;
        }

        public static void SetSharedVertexPosition(this MeshInspector mesh, int sharedVertexHandle, Vector3 position)
        {
            if (mesh == null)
                throw new ArgumentNullException("mesh");

            Vector3[] v = mesh.verticesPositions;

            foreach (var index in mesh.sharedVertices[sharedVertexHandle])
                v[index] = position;

            mesh.verticesPositions = v;
            mesh.mesh.vertices = v;
        }

        /// <summary>
        /// Set a collection of mesh attributes with a Vertex.
        /// <br /><br />
        /// Use @"UnityEngine.ProBuilder.ProBuilderMesh.sharedIndexes" and IntArrayUtility.IndexOf to get a shared (or common) index.
        /// </summary>
        /// <param name="mesh"></param>
        /// <param name="sharedVertexHandle"></param>
        /// <param name="vertex"></param>
        internal static void SetSharedVertexValues(this MeshInspector mesh, int sharedVertexHandle, Vertex vertex)
        {
            Vertex[] vertices = mesh.GetVertices();

            foreach (var index in mesh.sharedVertices[sharedVertexHandle])
                vertices[index] = vertex;

            mesh.SetVertices(vertices);
        }
    }
}
