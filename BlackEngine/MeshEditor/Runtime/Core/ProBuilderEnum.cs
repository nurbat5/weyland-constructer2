using UnityEngine;
using System.Collections;

namespace BlackEngine.MeshEditor.Runtime
{
    [System.Flags]
    public enum SelectMode
    {
        None = 0 << 0,
        Object = 1 << 0,
        Vertex = 1 << 1,
        Edge = 1 << 2,
        Face = 1 << 3,
        TextureFace = 1 << 4,
        TextureEdge = 1 << 5,
        TextureVertex = 1 << 6,
        InputTool = 1 << 7,
        Any = 0xFFFF
    }

    enum ComponentMode
    {
        Vertex = 0x0,
        Edge = 0x1,
        Face = 0x2
    }

    internal enum EditLevel
    {
        Top = 0,
        Geometry = 1,
        Texture = 2,
        Plugin = 3
    }

    enum EntityType
    {
        Detail,
        Occluder,
        Trigger,
        Collider,
        Mover
    }

    enum ColliderType
    {
        None,
        BoxCollider,
        MeshCollider
    }

    public enum ProjectionAxis
    {
        X,
        Y,
        Z,
        XNegative,
        YNegative,
        ZNegative
    }

    enum HandleAxis
    {
        X = 1 << 0,
        Y = 1 << 1,
        Z = 1 << 2,
        Free = 1 << 3
    }

    public enum Axis
    {
        Right,
        Left,
        Up,
        Down,
        Forward,
        Backward
    }

    public enum WindingOrder
    {
        Unknown,
        Clockwise,
        CounterClockwise
    }

    public enum SortMethod
    {
        Clockwise,
        CounterClockwise
    };

    [System.Flags]
    public enum CullingMode
    {
        None = 0 << 0,
        Back = 1 << 0,
        Front = 1 << 1,
        FrontBack = Front | Back,
    }

    public enum RectSelectMode
    {
        Partial,
        Complete
    }

    public enum MeshSyncState
    {
        Null,
        InstanceIDMismatch,
        Lightmap,
        InSync
    }

    [System.Flags]
    public enum MeshArrays
    {
        Position = 0x1,
        Texture0 = 0x2,
        Texture1 = 0x4,
        Lightmap = 0x4,
        Texture2 = 0x8,
        Texture3 = 0x10,
        Color = 0x20,
        Normal = 0x40,
        Tangent = 0x80,
        All = 0xFF
    };

    enum IndexFormat
    {
        Local = 0x0,
        Common = 0x1,
        Both = 0x2
    };

    [System.Flags]
    public enum RefreshMask
    {
        UV = 0x1,
        Colors = 0x2,
        Normals = 0x4,
        Tangents = 0x8,
        Collisions = 0x10,
        All = UV | Colors | Normals | Tangents | Collisions
    };

    public enum ExtrudeMethod
    {
        IndividualFaces = 0,
        VertexNormal = 1,
        FaceNormal = 2
    }
}
