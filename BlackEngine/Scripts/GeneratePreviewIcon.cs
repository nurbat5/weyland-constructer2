﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using static UnityEngine.Object;

namespace BlackEngine.Texture
{
    public class TextureScale
    {
        public class ThreadData
        {
            public int start;
            public int end;
            public ThreadData(int s, int e)
            {
                start = s;
                end = e;
            }
        }

        private static Color[] texColors;
        private static Color[] newColors;
        private static int w;
        private static float ratioX;
        private static float ratioY;
        private static int w2;
        private static int finishCount;
        private static Mutex mutex;

        public static void Point(Texture2D tex, int newWidth, int newHeight) => ThreadedScale(tex, newWidth, newHeight, false);
        public static void Bilinear(Texture2D tex, int newWidth, int newHeight) => ThreadedScale(tex, newWidth, newHeight, true);
        private static void ThreadedScale(Texture2D tex, int newWidth, int newHeight, bool useBilinear)
        {
            texColors = tex.GetPixels();
            newColors = new Color[newWidth * newHeight];
            if (useBilinear)
            {
                ratioX = 1.0f / ((float)newWidth / (tex.width - 1));
                ratioY = 1.0f / ((float)newHeight / (tex.height - 1));
            }
            else
            {
                ratioX = ((float)tex.width) / newWidth;
                ratioY = ((float)tex.height) / newHeight;
            }
            w = tex.width;
            w2 = newWidth;
            var cores = Mathf.Min(SystemInfo.processorCount, newHeight);
            var slice = newHeight / cores;

            finishCount = 0;
            if (mutex == null) mutex = new Mutex(false);
            if (cores > 1)
            {
                int i = 0;
                ThreadData threadData;
                for (i = 0; i < cores - 1; i++)
                {
                    threadData = new ThreadData(slice * i, slice * (i + 1));
                    ParameterizedThreadStart ts = useBilinear ? new ParameterizedThreadStart(BilinearScale) : new ParameterizedThreadStart(PointScale);
                    Thread thread = new Thread(ts);
                    thread.Start(threadData);
                }
                threadData = new ThreadData(slice * i, newHeight);
                if (useBilinear) BilinearScale(threadData);
                else PointScale(threadData);
                while (finishCount < cores) Thread.Sleep(1);
            }
            else
            {
                ThreadData threadData = new ThreadData(0, newHeight);
                if (useBilinear) BilinearScale(threadData);
                else PointScale(threadData);
            }

            tex.Resize(newWidth, newHeight);
            tex.SetPixels(newColors);
            tex.Apply();

            texColors = null;
            newColors = null;
        }

        public static void BilinearScale(System.Object obj)
        {
            ThreadData threadData = (ThreadData)obj;
            for (var y = threadData.start; y < threadData.end; y++)
            {
                int yFloor = (int)Mathf.Floor(y * ratioY);
                var y1 = yFloor * w;
                var y2 = (yFloor + 1) * w;
                var yw = y * w2;

                for (var x = 0; x < w2; x++)
                {
                    int xFloor = (int)Mathf.Floor(x * ratioX);
                    var xLerp = x * ratioX - xFloor;
                    newColors[yw + x] = ColorLerpUnclamped(ColorLerpUnclamped(texColors[y1 + xFloor], texColors[y1 + xFloor + 1], xLerp),
                                                           ColorLerpUnclamped(texColors[y2 + xFloor], texColors[y2 + xFloor + 1], xLerp),
                                                           y * ratioY - yFloor);
                }
            }

            mutex.WaitOne();
            finishCount++;
            mutex.ReleaseMutex();
        }

        public static void PointScale(System.Object obj)
        {
            ThreadData threadData = (ThreadData)obj;
            for (var y = threadData.start; y < threadData.end; y++)
            {
                var thisY = (int)(ratioY * y) * w;
                var yw = y * w2;
                for (var x = 0; x < w2; x++)
                {
                    newColors[yw + x] = texColors[(int)(thisY + ratioX * x)];
                }
            }

            mutex.WaitOne();
            finishCount++;
            mutex.ReleaseMutex();
        }

        private static Color ColorLerpUnclamped(Color c1, Color c2, float value)
        {
            return new Color(c1.r + (c2.r - c1.r) * value,
                              c1.g + (c2.g - c1.g) * value,
                              c1.b + (c2.b - c1.b) * value,
                              c1.a + (c2.a - c1.a) * value);
        }
    }

    [Serializable]
    public struct OtherSettings
    {
        public Texture2D backgroundImage;
        // public string backgroundUri;
        public bool cloneModel;
        public Shader shader;
        public Material material;
        public Transform model;
        public Vector2 iconSize;

        //Camera offset Direction (Normal)
        private Vector3 m_PreviewDirection;
        public Vector3 direction
        {
            get { return m_PreviewDirection; }
            set { m_PreviewDirection = value.normalized; }
        }

        //Create primitive Object
        [NonSerialized] public bool m_IsPrimitive;
        private PrimitiveType? m_PrimitiveType;
        public PrimitiveType? primitiveType
        {
            get => m_IsPrimitive ? (PrimitiveType)m_PrimitiveType : PrimitiveType.Cube;
            set => m_IsPrimitive = (m_PrimitiveType = value) != null ? true : false;
        }

        public void Setup(OtherSettings? settings)
        {
            iconSize = new Vector2(256, 256);
            primitiveType = null;
            m_PreviewDirection = new Vector3(-1f, -1f, -1f);
            cloneModel = false;
            if (settings != null) LoadSettings((OtherSettings)settings);
        }
        public void LoadSettings(OtherSettings settings)
        {
            m_IsPrimitive = settings.m_IsPrimitive;
            m_PrimitiveType = settings.m_PrimitiveType;
            model = settings.model;
            cloneModel = settings.cloneModel;
        }
    }

    [Serializable]
    public struct CameraSettings
    {
        [Range(-1, 1)]
        public float orthographicPadding;
        public bool orthographicMode;
        public bool allowHDR;
        public bool allowMSAA;
        public bool allowDynamicResolution;
        public bool transparentBackground;
        public Color backgroundColor;
        public Camera renderCamera;
        public int layer;
        public void Setup(CameraSettings? settings)
        {
            PressetCamera();
            if (renderCamera != null)
            {
                GetSettings(renderCamera);
                renderCamera.nearClipPlane = 0.01f;
            }
            else renderCamera = GeneratePreviewEngine.mInternalCamera;
            if (settings != null) LoadSettings((CameraSettings)settings);
        }

        #region privateProperty
        private bool m_AllowHDR;
        private bool m_AllowMSAA;
        private bool m_AllowDynamicResolution;
        private Color m_BackgroundColor;
        private Vector3 m_Position;
        private Quaternion m_Rotation;
        private RenderTexture m_TargetTexture;
        private bool m_Orthographic;
        private float m_OrthographicSize;
        private float m_NearClipPlane;
        private float m_FarClipPlane;
        private float m_Aspect;
        private CameraClearFlags m_ClearFlags;
        private void Setup(Camera camera)
        {
            camera.transform.position = m_Position;
            camera.transform.rotation = m_Rotation;
            camera.targetTexture = m_TargetTexture;
            camera.backgroundColor = m_BackgroundColor;
            camera.orthographic = m_Orthographic;
            camera.orthographicSize = m_OrthographicSize;
            camera.nearClipPlane = m_NearClipPlane;
            camera.farClipPlane = m_FarClipPlane;
            camera.aspect = m_Aspect;
            camera.clearFlags = m_ClearFlags;
            camera.allowDynamicResolution = m_AllowDynamicResolution;
            camera.allowHDR = m_AllowHDR;
            camera.allowMSAA = m_AllowMSAA;
            m_TargetTexture = null;
        }
        private void GetSettings(Camera camera)
        {
            m_Position = camera.transform.position;
            m_Rotation = camera.transform.rotation;
            m_TargetTexture = camera.targetTexture;
            m_BackgroundColor = camera.backgroundColor;
            m_Orthographic = camera.orthographic;
            m_OrthographicSize = camera.orthographicSize;
            m_NearClipPlane = camera.nearClipPlane;
            m_FarClipPlane = camera.farClipPlane;
            m_Aspect = camera.aspect;
            m_ClearFlags = camera.clearFlags;
            m_AllowHDR = camera.allowHDR;
            m_AllowMSAA = camera.allowMSAA;
            m_AllowDynamicResolution = camera.allowDynamicResolution;
        }
        public void LoadSettings(CameraSettings settings)
        {
            orthographicPadding = settings.orthographicPadding;
            renderCamera.allowDynamicResolution = settings.allowDynamicResolution;
            renderCamera.allowHDR = settings.allowHDR;
            renderCamera.allowMSAA = settings.allowMSAA;
            renderCamera.backgroundColor = settings.backgroundColor;
            renderCamera.orthographic = settings.orthographicMode;
            transparentBackground = settings.transparentBackground;
            renderCamera.clearFlags = settings.transparentBackground ? CameraClearFlags.Depth : CameraClearFlags.Color;
            renderCamera.cullingMask = 1 << settings.layer;
        }
        private void PressetCamera()
        {
            transparentBackground = false;
            orthographicPadding = 0;
            orthographicMode = true;
            allowHDR = allowMSAA = false;
            allowDynamicResolution = false;
            backgroundColor = new Color(0.3f, 0.3f, 0.3f, 1f);
            layer = 22;
        }
        #endregion
    }

    public class GeneratePreviewEngine
    {
        public class ResultEvent : UnityEvent<Texture2D> { }

        #region privateProperty
        private const int HASH_LAYER = 22;
        private List<Renderer> m_RenderersList = new List<Renderer>(64);
        private float m_Aspect;
        private float m_MinX, m_MaxX, m_MinY, m_MaxY;
        private float m_MaxDistance;
        private Vector3 m_BoundsCenter;
        private Plane m_ProjectionPlaneHorizontal, m_ProjectionPlaneVertical;
        private String m_ReplacementTag = null;
        private Transform m_ModelObject;
        private Texture2D m_Result;
        #endregion

        //Default Render Camera
        private static bool m_IsInternalCamera = false;
        private static Camera m_InternalCamera;
        protected internal static Camera mInternalCamera
        {
            get
            {
                if (!m_IsInternalCamera)
                {
                    m_InternalCamera = new GameObject("ModelPreviewGeneratorCamera").AddComponent<Camera>();
                    m_InternalCamera.enabled = false;
                    m_InternalCamera.nearClipPlane = 0.01f;
                    m_InternalCamera.cullingMask = 1 << HASH_LAYER;
                    m_InternalCamera.gameObject.hideFlags = HideFlags.HideAndDontSave;
                    m_IsInternalCamera = true;
                }
                return m_InternalCamera;
            }
        }

        //Custom Render Camera
        private bool m_IsRenderCamera = false;
        private Camera m_RenderCamera
        {
            get
            {
                if (!m_IsRenderCamera) return m_InternalCamera;
                return renderConfig.renderCamera;
            }
            set
            {
                m_IsRenderCamera = true;
                renderConfig.renderCamera = value;
            }
        }

        //Result Texture and Event
        public ResultEvent RenderReady = new ResultEvent();
        public OtherSettings config;//Other Settings
        public CameraSettings renderConfig = new CameraSettings();//Render property

        #region Fast Presset config
        public GeneratePreviewEngine()
        {
            config.Setup(null);
            renderConfig.Setup(null);
            config.material = new Material(Shader.Find("Legacy Shaders/Diffuse"))
            {
                hideFlags = HideFlags.HideAndDontSave,
                color = new Color(0f, 1f, 1f, 1f)
            };
        }
        public GeneratePreviewEngine(PrimitiveType primitiveType) : this(primitiveType, null) { }
        public GeneratePreviewEngine(PrimitiveType primitiveType, Material material) : this()
        {
            config.primitiveType = primitiveType;
            config.material = material;
        }
        public GeneratePreviewEngine(Transform model, Material material) : this()
        {
            config.model = model;
            config.material = material;
        }
        public GeneratePreviewEngine(Transform model) : this(model, null) { }
        #endregion

        #region Calculate
        private void PreRender()
        {
            config.Setup(config);
            renderConfig.Setup(renderConfig);
            m_Result = null;
            if (config.m_IsPrimitive)
            {
                m_ModelObject = GameObject.CreatePrimitive((PrimitiveType)config.primitiveType).transform;
                m_ModelObject.gameObject.hideFlags = HideFlags.HideAndDontSave;
                config.model = m_ModelObject.transform;
            }
            if (config.model.Equals(null)) throw new Exception("[GeneratePreviewIcon] Model object not setup.");
            if (!config.model.gameObject.scene.IsValid() || !config.model.gameObject.scene.isLoaded) config.cloneModel = true;
            if (config.cloneModel)
            {
                m_ModelObject = Instantiate(config.model, null, false);
                m_ModelObject.gameObject.hideFlags = HideFlags.HideAndDontSave;
            }
            else m_ModelObject = config.model;
            SetLayer(m_ModelObject);
            SetRenderer(m_ModelObject);
        }
        float rotate = 0;
        public Texture2D Render()
        {
            PreRender();
            try
            {
                rotate += 15f * Time.deltaTime;
                m_ModelObject.Rotate(Vector3.up, rotate);
                Vector3 previewDir = m_ModelObject.rotation * config.direction;
                m_RenderersList.Clear();
                m_ModelObject.GetComponentsInChildren(m_RenderersList);

                Bounds previewBounds = new Bounds();
                bool init = false;
                for (int i = 0; i < m_RenderersList.Count; i++)
                {
                    if (!m_RenderersList[i].enabled) continue;
                    if (!init)
                    {
                        previewBounds = m_RenderersList[i].bounds;
                        init = true;
                    }
                    else previewBounds.Encapsulate(m_RenderersList[i].bounds);
                }
                if (!init) throw new Exception("[GeneratePreviewIcon : 269]");

                m_BoundsCenter = previewBounds.center;
                Vector3 boundsExtents = previewBounds.extents;
                Vector3 boundsSize = 2f * boundsExtents;

                m_Aspect = (float)config.iconSize.x / config.iconSize.y;
                m_RenderCamera.aspect = m_Aspect;
                m_RenderCamera.transform.rotation = Quaternion.LookRotation(previewDir, m_ModelObject.up);

                float distance;
                if (renderConfig.orthographicMode)
                {
                    m_RenderCamera.transform.position = m_BoundsCenter;

                    m_MinX = m_MinY = Mathf.Infinity;
                    m_MaxX = m_MaxY = Mathf.NegativeInfinity;
                    RecalculateProjectBoundingBoxMinMax(m_BoundsCenter, boundsExtents, boundsSize);
                    distance = boundsExtents.magnitude + 1f;
                    m_RenderCamera.orthographicSize = (1f + renderConfig.orthographicPadding * 2f) * Mathf.Max(m_MaxY - m_MinY, (m_MaxX - m_MinX) / m_Aspect) * 0.5f;
                }
                else
                {
                    m_ProjectionPlaneHorizontal = new Plane(m_RenderCamera.transform.up, m_BoundsCenter);
                    m_ProjectionPlaneVertical = new Plane(m_RenderCamera.transform.right, m_BoundsCenter);

                    m_MaxDistance = Mathf.NegativeInfinity;
                    RecalculateMaxDistance(m_BoundsCenter, boundsExtents, boundsSize);
                    distance = (1f + renderConfig.orthographicPadding * 2f) * Mathf.Sqrt(m_MaxDistance);
                }

                m_RenderCamera.transform.position = m_BoundsCenter - previewDir * distance;
                m_RenderCamera.farClipPlane = distance * 4f;



                RenderTexture temp = RenderTexture.active;
                RenderTexture renderTex = RenderTexture.GetTemporary((int)config.iconSize.x, (int)config.iconSize.y, 16);
                RenderTexture.active = renderTex;

                if (renderConfig.transparentBackground) GL.Clear(false, true, Color.clear);

                m_RenderCamera.targetTexture = renderTex;

                if (config.shader == null) m_RenderCamera.Render();
                else m_RenderCamera.RenderWithShader(config.shader, m_ReplacementTag == null ? string.Empty : m_ReplacementTag);

                m_RenderCamera.targetTexture = null;

                m_Result = new Texture2D((int)config.iconSize.x, (int)config.iconSize.y, renderConfig.transparentBackground ? TextureFormat.RGBA32 : TextureFormat.RGB24, false);
                m_Result.name = config.model.name;
                m_Result.ReadPixels(new Rect(0, 0, (int)config.iconSize.x, (int)config.iconSize.y), 0, 0, false);
                CombineTextures();
                RenderReady.Invoke(m_Result);
                m_Result.Apply(false, true);

                RenderTexture.active = temp;
                RenderTexture.ReleaseTemporary(renderTex);
            }
            catch (Exception e) { Debug.LogException(e); }
            finally { DestroyImmediate(m_ModelObject.gameObject); }
            return m_Result;
        }

        private void CombineTextures()
        {
            if (!renderConfig.transparentBackground || config.backgroundImage == null) return;
            var offset = new Vector2(((m_Result.width - m_Result.width) / 2), ((m_Result.height - config.backgroundImage.height) / 2));
            //m_Result.SetPixels(config.backgroundImage.GetPixels());
            for (int y = 0; y < config.backgroundImage.height; y++)
            {
                Debug.Log(m_Result.GetPixel(0, y).a);
                for (int x = 0; x < config.backgroundImage.width; x++)
                {
                    Color PixelColorFore = m_Result.GetPixel(x, y) * m_Result.GetPixel(x, y).a;
                    Color PixelColorBack = config.backgroundImage.GetPixel((int)x + (int)offset.x, y + (int)offset.y) * (1 - PixelColorFore.a);
                    m_Result.SetPixel((int)x + (int)offset.x, (int)y + (int)offset.y, PixelColorBack + PixelColorFore);
                }
            }
            //m_Result.Apply();
        }

        private void RecalculateProjectBoundingBoxMinMax(Vector3 m_BoundsCenter, Vector3 boundsExtents, Vector3 boundsSize)
        {
            Vector3 point = m_BoundsCenter + boundsExtents;
            ProjectBoundingBoxMinMax(point);
            point.x -= boundsSize.x;
            ProjectBoundingBoxMinMax(point);
            point.y -= boundsSize.y;
            ProjectBoundingBoxMinMax(point);
            point.x += boundsSize.x;
            ProjectBoundingBoxMinMax(point);
            point.z -= boundsSize.z;
            ProjectBoundingBoxMinMax(point);
            point.x -= boundsSize.x;
            ProjectBoundingBoxMinMax(point);
            point.y += boundsSize.y;
            ProjectBoundingBoxMinMax(point);
            point.x += boundsSize.x;
            ProjectBoundingBoxMinMax(point);
        }
        private void ProjectBoundingBoxMinMax(Vector3 point)
        {
            Vector3 localPoint = m_RenderCamera.transform.InverseTransformPoint(point);
            if (localPoint.x < m_MinX) m_MinX = localPoint.x;
            if (localPoint.x > m_MaxX) m_MaxX = localPoint.x;
            if (localPoint.y < m_MinY) m_MinY = localPoint.y;
            if (localPoint.y > m_MaxY) m_MaxY = localPoint.y;
        }
        private void RecalculateMaxDistance(Vector3 m_BoundsCenter, Vector3 boundsExtents, Vector3 boundsSize)
        {
            Vector3 point = m_BoundsCenter + boundsExtents;
            CalculateMaxDistance(point);
            point.x -= point.x - boundsSize.x;
            CalculateMaxDistance(point);
            point.y -= boundsSize.y;
            CalculateMaxDistance(point);
            point.x += boundsSize.x;
            CalculateMaxDistance(point);
            point.z -= boundsSize.z;
            CalculateMaxDistance(point);
            point.x -= boundsSize.x;
            CalculateMaxDistance(point);
            point.y += boundsSize.y;
            CalculateMaxDistance(point);
            point.x += boundsSize.x;
            CalculateMaxDistance(point);
        }
        private void CalculateMaxDistance(Vector3 point)
        {
            Vector3 intersectionPoint = m_ProjectionPlaneHorizontal.ClosestPointOnPlane(point);

            float horizontalDistance = m_ProjectionPlaneHorizontal.GetDistanceToPoint(point);
            float verticalDistance = m_ProjectionPlaneVertical.GetDistanceToPoint(point);

            // Credit: https://docs.unity3d.com/Manual/FrustumSizeAtDistance.html
            float halfFrustumHeight = Mathf.Max(verticalDistance, horizontalDistance / m_Aspect);
            float distance = halfFrustumHeight / Mathf.Tan(m_RenderCamera.fieldOfView * 0.5f * Mathf.Deg2Rad);

            float distanceToCenter = (intersectionPoint - config.direction * distance - m_BoundsCenter).sqrMagnitude;
            if (distanceToCenter > m_MaxDistance) m_MaxDistance = distanceToCenter;
        }
        private void SetRenderer(Transform obj)
        {
            try
            {
                if (config.material != null) obj.GetComponent<Renderer>().sharedMaterial = config.material;
                obj.GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            }
            catch { }
            for (int i = 0; i < obj.childCount; i++) SetRenderer(obj.GetChild(i));
        }
        private void SetLayer(Transform obj)
        {
            obj.gameObject.layer = renderConfig.layer;
            for (int i = 0; i < obj.childCount; i++) SetLayer(obj.GetChild(i));
        }
        #endregion
    }

    public class GeneratePreviewIcon : CoreEngine
    {
        [Header("Save to File")]
        public bool save = false;
        public string customFolder = null;

        [Header("Debug")]
        [Space(20)]
        public bool Generate = false;
        public Texture2D resultRender;

        [Header("Config")]
        [Space(20)]
        public CameraSettings cameraSettings = new CameraSettings
        {
            transparentBackground = false,
            orthographicPadding = 0,
            orthographicMode = true,
            allowHDR = false,
            allowMSAA = false,
            allowDynamicResolution = false,
            renderCamera = null,
            backgroundColor = new Color(0.3f, 0.3f, 0.3f, 1f),
            layer = 22
        };
        public OtherSettings otherSettings = new OtherSettings
        {
            iconSize = new Vector2(256, 256),
            cloneModel = false
        };

        private GeneratePreviewEngine previewEngine;
        void Start()
        {
            previewEngine = new GeneratePreviewEngine();
            previewEngine.renderConfig = cameraSettings;
            previewEngine.config = otherSettings;
            previewEngine.RenderReady.AddListener(SaveIcon);
            resultRender = previewEngine.Render();
            Debug.Log("OK");
        }

        private void SaveIcon(Texture2D arg0)
        {
            if (!save) return;
            if (customFolder == "") // Check if custom folder is specified!
            {
                File.WriteAllBytes(Application.dataPath + "/" + arg0.name + ".png", arg0.EncodeToPNG());
                Debug.Log("File saved in: " + Application.dataPath + "/" + arg0.name + ".png");
            }
            else
            {
                File.WriteAllBytes(Application.dataPath + "/" + customFolder + "/" + arg0.name + ".png", arg0.EncodeToPNG());
                Debug.Log("File saved in: " + Application.dataPath + "/" + customFolder + "/" + arg0.name + ".png");
            }
        }

        void FixedUpdate()
        {
            if (Generate)
            {
                previewEngine.renderConfig = cameraSettings;
                previewEngine.config = otherSettings;

                resultRender = previewEngine.Render();
                Generate = false;
            }
        }
    }
}

            // if (config.backgroundImage == null && !(config.backgroundUri.Length > 5)) return;
            // if (config.backgroundUri.Length > 5)
            // {
            //     byte[] fileData = File.ReadAllBytes(Application.dataPath.Replace("Assets", "") + config.backgroundUri);
            //     overlay = new Texture2D(2, 2);
            //     overlay.LoadImage(fileData);
            //     if (overlay.height != config.iconSize.x) TextureScale.Point(overlay, (int)config.iconSize.x, (int)config.iconSize.y);
            //     overlay.LoadImage(overlay.EncodeToPNG());
            // }