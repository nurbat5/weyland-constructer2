using UnityEngine;

public interface ICoreEngine
{
    GameObject gameObject { get; }
    Transform transform { get; }
    Rigidbody rigidbody { get; }
    Animation animation { get; }
    Camera camera { get; }
    MeshRenderer renderer { get; }
    MeshFilter meshFilter { get; }
    Mesh mesh { get; }
}