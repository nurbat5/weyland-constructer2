﻿using System;
using Unity.Mathematics;
using UnityEngine;

public class ReadOnlyAttribute : PropertyAttribute { }
public class CoreEngine : MonoBehaviour, ICoreEngine
{
    #region Cached base class
    [NonSerialized] private bool m_IsGameObject;
    [NonSerialized] private GameObject m_GameObject;
    public new GameObject gameObject 
    {
        get 
        {
            if(m_IsGameObject) return m_GameObject;
            m_IsGameObject = true;
            return m_GameObject = base.gameObject;
        }
    }

    [NonSerialized] private bool m_IsTransform;
    [NonSerialized] private Transform m_Transform;
    public new Transform transform    
    {
        get 
        {
            if(m_IsTransform) return m_Transform;
            m_IsTransform = true;
            return m_Transform = base.transform;
        }
    }

    [NonSerialized] private bool m_IsRigidbody;
    [NonSerialized] private Rigidbody m_Rigidbody;
    public new Rigidbody rigidbody
    {
        get 
        {
            if(m_IsRigidbody) return m_Rigidbody;
            m_IsRigidbody = true;
            return m_Rigidbody = GetComponent<Rigidbody>();
        }
    }

    [NonSerialized] private bool m_IsAnimation;
    [NonSerialized] private Animation m_Animation;
    public new Animation animation
    {
        get 
        {
            if(m_IsAnimation) return m_Animation;
            m_IsAnimation = true;
            return m_Animation = GetComponent<Animation>();
        }
    }

    [NonSerialized] private bool m_IsCamera;
    [NonSerialized] private Camera m_Camera;
    public new Camera camera
    {
        get 
        {
            if(m_IsCamera) return m_Camera;
            m_IsCamera = true;
            return m_Camera = Camera.main;
        }
    }

    [NonSerialized] private bool m_IsMeshRenderer;
    [NonSerialized] private MeshRenderer m_MeshRenderer;
    public new MeshRenderer renderer
    {
        get 
        {
            if(m_IsMeshRenderer) return m_MeshRenderer;
            m_IsMeshRenderer = true;
            return m_MeshRenderer = GetComponent<MeshRenderer>();
        }
    }

    [NonSerialized] private bool m_IsMeshFilter;
    [NonSerialized] private MeshFilter m_MeshFilter;
    public MeshFilter meshFilter
    {
        get 
        {
            if(m_IsMeshFilter) return m_MeshFilter;
            m_IsMeshFilter = true;
            return m_MeshFilter = GetComponent<MeshFilter>();
        }
    }

    [NonSerialized] private bool m_IsMesh;
    [NonSerialized] private Mesh m_Mesh;
    public Mesh mesh
    {
        get 
        {
            if(m_IsMesh) return m_Mesh;
            m_IsMesh = true;
            return m_Mesh = meshFilter.mesh;
        }
        set => meshFilter.mesh = m_Mesh = value;
    }
    #endregion

    public static float3 FindCenterMass(float3[] pos, float[] mass)
    {
        float3 centerMass = new float3();
        float sumMass = 0;
        for (int i = 0; i < pos.Length; i++)
        {
            centerMass.x += pos[i].x * mass[i];
            centerMass.y += pos[i].y * mass[i];
            centerMass.z += pos[i].z * mass[i];
            sumMass += mass[i];
        }
        return centerMass /= sumMass;
    }
    public static void FindCenterMass(float3[] pos, float[] mass, out float3 centerMass, out float lenghtFirstPoint)
    {
        lenghtFirstPoint = 0;
        centerMass = FindCenterMass(pos, mass);
        foreach (float3 dot in pos)
        {
            lenghtFirstPoint = (float)Math.Sqrt((dot.x - centerMass.x) * (dot.x - centerMass.x) + (dot.y - centerMass.y) * (dot.y - centerMass.y) + (dot.z - centerMass.z) * (dot.z - centerMass.z));
        }
    }
}
