using UnityEngine;
using UnityEngine.Experimental.Input;

namespace BlackEngine
{
    public class ScreenStick : CoreEngine
    {
        private void Start()
        {
            m_StartPos = ((RectTransform)transform).anchoredPosition;
        }

        private void Update()
        {
            if(!Mouse.current.leftButton.isPressed) return;
            OnMouseDown();
            OnMove();
        }

        public void OnMouseDown()
        {
            if(Mouse.current.leftButton.wasPressedThisFrame) 
                RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponentInParent<RectTransform>(), Mouse.current.position.ReadValue(), camera, out m_PointerDownPos);
        }

        public void OnMove()
        {
            Vector2 position;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponentInParent<RectTransform>(), Mouse.current.position.ReadValue(), camera, out position);
            var delta = position - m_PointerDownPos;
            delta = Vector2.ClampMagnitude(delta, movementRange);
            ((RectTransform)transform).anchoredPosition = m_StartPos + (Vector3)delta;
        }

        // ((RectTransform)transform).anchoredPosition = m_StartPos;

        public int movementRange = 50;

        private Vector3 m_StartPos;
        private Vector2 m_PointerDownPos;
    }
}