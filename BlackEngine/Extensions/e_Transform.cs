﻿using Unity.Mathematics;
using UnityEngine;

public static class E_Transform
{
    public static float3 SetWorldPivotPoint(this Transform transform, float3 localPivot) => transform.rotation * (transform.position - (Vector3)localPivot);
    public static float3 SetLocalPivotPoint(this Transform transform, float3 localPivot) => transform.rotation * localPivot;
}
