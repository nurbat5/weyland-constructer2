﻿using System;
using UnityEngine;

namespace BlackEngine.CharacterController
{
    [RequireComponent(typeof(Rigidbody)), RequireComponent(typeof(CapsuleCollider))]
    public class CharacterMotor : CoreEngine
    {
        [Header("Capsule Settings")]

        [SerializeField]
        [Tooltip("Radius of the Character Capsule")]
        private float CapsuleRadius = 0.5f;

        [SerializeField]
        [Tooltip("Height of the Character Capsule")]
        private float CapsuleHeight = 2f;

        [SerializeField]
        [Tooltip("Height of the Character Capsule")]
        private float CapsuleYOffset = 1f;

        [Tooltip("Physics material of the Character Capsule (Does not affect character movement. Only affects things colliding with it)")]
        public PhysicMaterial capsulePhysicsMaterial;

        [Header("Misc Options")]

        [Tooltip("Notifies the Character Controller when discrete collisions are detected")]
        public bool DetectDiscreteCollisions = false;

        [Tooltip("Increases the range of ground detection, to allow snapping to ground at very high speeds")]
        public float GroundDetectionExtraDistance = 0f;
        
        [Tooltip("Maximum height of a step which the character can climb")]
        public float MaxStepHeight = 0.5f;

        [Tooltip("Minimum length of a step that the character can step on (used in Extra stepping method). Use this to let the character step on steps that are smaller that its radius")]
        public float MinRequiredStepDepth = 0.1f;

        [Range(0f, 89f)]
        [Tooltip("Maximum slope angle on which the character can be stable")]
        public float MaxStableSlopeAngle = 60f;

        [Tooltip("The distance from the capsule central axis at which the character can stand on a ledge and still be stable")]
        public float MaxStableDistanceFromLedge = 0.5f;

        [Tooltip("Prevents snapping to ground on ledges. Set this to true if you want more determinism when launching off slopes")]
        public bool PreventSnappingOnLedges = false;

        [Tooltip("The maximun downward slope angle change that the character can be subjected to and still be snapping to the ground")]
        [Range(1f, 180f)]
        public float MaxStableDenivelationAngle = 180f;

        [Header("Rigidbody interactions")]

        [Tooltip("How the character interacts with non-kinematic rigidbodies. \"Kinematic\" mode means the character pushes the rigidbodies with infinite force (as a kinematic body would). \"SimulatedDynamic\" pushes the rigidbodies with a simulated mass value.")]
        public RigidbodyInteractionType RigidbodyInteractionType;

        [Tooltip("Determines if the character preserves moving platform velocities when de-grounding from them")]
        public bool PreserveAttachedRigidbodyMomentum = true;

        [Header("Constraints")]

        [Tooltip("Determines if the character's movement uses the planar constraint")]
        public bool HasPlanarConstraint = false;

        [Tooltip("Defines the plane that the character's movement is constrained on, if HasMovementConstraintPlane is active")]
        public Vector3 PlanarConstraintAxis = Vector3.forward;

        [Header("Features & Optimizations")]

        [Tooltip("Handles properly detecting grounding status on steps, but has a performance cost.")]
        public StepHandlingMethod StepHandling = StepHandlingMethod.Standard;

        [Tooltip("Handles properly detecting ledge information and grounding status, but has a performance cost.")]
        public bool LedgeHandling = true;

        [Tooltip("Handles properly being pushed by and standing on PhysicsMovers or dynamic rigidbodies. Also handles pushing dynamic rigidbodies")]
        public bool InteractiveRigidbodyHandling = true;

        [Tooltip("(We suggest leaving this off. This has a pretty heavy performance cost, and is not necessary unless you start seeing situations where a fast-moving character moves through colliders) Makes sure the character cannot perform a move at all if it would be overlapping with any collidable objects at its destination. Useful for preventing \"tunneling\". ")]
        public bool SafeMovement = true;


        [NonSerialized]
        public GroundingReport GroundingStatus = new GroundingReport();

        [NonSerialized]
        public TransientGroundingReport LastGroundingStatus = new TransientGroundingReport();

        [NonSerialized]
        public LayerMask CollidableLayers = -1;

        public Vector3 CharacterUp { get; private set; }
        public Vector3 CharacterForward { get; private set; }
        public Vector3 CharacterRight { get; private set; }
        public Vector3 InitialSimulationPosition { get; private set; }
        public Quaternion InitialSimulationRotation { get; private set; }
        public Rigidbody AttachedRigidbody { get; private set; }
        public Vector3 CharacterTransformToCapsuleCenter { get; private set; }
        public Vector3 CharacterTransformToCapsuleBottom { get; private set; }
        public Vector3 CharacterTransformToCapsuleTop { get; private set; }
        public Vector3 CharacterTransformToCapsuleBottomHemi { get; private set; }
        public Vector3 CharacterTransformToCapsuleTopHemi { get; private set; }
        public bool MustUnground { get; set; }
        public bool LastMovementIterationFoundAnyGround { get; set; }
        public int IndexInCharacterSystem { get; set; }
        public Vector3 InitialTickPosition { get; set; }
        public Quaternion InitialTickRotation { get; set; }
        public Rigidbody AttachedRigidbodyOverride { get; set; }
        private RaycastHit[] _internalCharacterHits = new RaycastHit[MaxHitsBudget];
        private Collider[] _internalProbedColliders = new Collider[MaxCollisionBudget];
        private Rigidbody[] _rigidbodiesPushedThisMove = new Rigidbody[MaxCollisionBudget];
        private RigidbodyProjectionHit[] _internalRigidbodyProjectionHits = new RigidbodyProjectionHit[MaxMovementSweepIterations];
        private Rigidbody _lastAttachedRigidbody;
        private bool _solveMovementCollisions = true;
        private bool _solveGrounding = true;
        private bool _movePositionDirty = false;
        private Vector3 _movePositionTarget = Vector3.zero;
        private bool _moveRotationDirty = false;
        private Quaternion _moveRotationTarget = Quaternion.identity;
        private Vector3 _lastSolvedOverlapNormal = Vector3.forward;
        private int _rigidbodiesPushedCount = 0;
        private int _rigidbodyProjectionHitCount = 0;
        private float _internalResultingMovementMagnitude = 0f;
        private Vector3 _internalResultingMovementDirection = Vector3.zero;
        private bool _isMovingFromAttachedRigidbody = false;
        private Vector3 _cachedWorldUp = Vector3.up;
        private Vector3 _cachedWorldForward = Vector3.forward;
        private Vector3 _cachedWorldRight = Vector3.right;
        private Vector3 _cachedZeroVector = Vector3.zero;

        private Vector3 _internalTransientPosition;
        public Vector3 TransientPosition
        {
            get => _internalTransientPosition;
            private set => _internalTransientPosition = value;
        }

        private Quaternion _internalTransientRotation;
        public Quaternion TransientRotation
        {
            get => _internalTransientRotation;
            private set
            {
                _internalTransientRotation = value;
                CharacterUp = _internalTransientRotation * _cachedWorldUp;
                CharacterForward = _internalTransientRotation * _cachedWorldForward;
                CharacterRight = _internalTransientRotation * _cachedWorldRight;
            }
        }


        public Vector3 InterpolatedPosition => transform.position;
        public Quaternion InterpolatedRotation => transform.rotation;
        public Vector3 Velocity => BaseVelocity + AttachedRigidbodyVelocity;

        private Vector3 _baseVelocity;
        public Vector3 BaseVelocity { get => _baseVelocity; set => _baseVelocity = value; }
        private Vector3 _attachedRigidbodyVelocity;
        public Vector3 AttachedRigidbodyVelocity { get => _attachedRigidbodyVelocity; set => _attachedRigidbodyVelocity = value; }
        private int _overlapsCount;
        public int OverlapsCount { get => _overlapsCount; private set => _overlapsCount = value; }

        public OverlapResult[] Overlaps { get; } = new OverlapResult[MaxRigidbodyOverlapsCount];

        // Warning: Don't touch these constants unless you know exactly what you're doing!
        public const int MaxHitsBudget = 16;
        public const int MaxCollisionBudget = 16;
        public const int MaxGroundingSweepIterations = 2;
        public const int MaxMovementSweepIterations = 6;
        public const int MaxSteppingSweepIterations = 3;
        public const int MaxRigidbodyOverlapsCount = 16;
        public const int MaxDiscreteCollisionIterations = 3;
        public const float CollisionOffset = 0.001f;
        public const float GroundProbeReboundDistance = 0.02f;
        public const float MinimumGroundProbingDistance = 0.005f;
        public const float GroundProbingBackstepDistance = 0.1f;
        public const float SweepProbingBackstepDistance = 0.002f;
        public const float SecondaryProbesVertical = 0.02f;
        public const float SecondaryProbesHorizontal = 0.001f;
        public const float MinVelocityMagnitude = 0.01f;
        public const float SteppingForwardDistance = 0.03f;
        public const float MinDistanceForLedge = 0.05f;
        public const float CorrelationForVerticalObstruction = 0.01f;
        public const float ExtraSteppingForwardDistance = 0.01f;
        public const float ExtraStepHeightPadding = 0.01f;

        #region InterpolationUser
        [SerializeField]
        private CharacterSystemInterpolationMethod _internalInterpolationMethod = CharacterSystemInterpolationMethod.Custom;
        public CharacterSystemInterpolationMethod InterpolationMethod
        {
            get => _internalInterpolationMethod;
            set
            {
                _internalInterpolationMethod = value;

                float interpolationFactor = Mathf.Clamp01((Time.time - _lastCustomInterpolationStartTime) / _lastCustomInterpolationDeltaTime);
                transform.SetPositionAndRotation(Vector3.Lerp(InitialTickPosition, TransientPosition, interpolationFactor),
                    Quaternion.Slerp(InitialTickRotation, TransientRotation, interpolationFactor));

                RigidbodyInterpolation interpMethod = (_internalInterpolationMethod == CharacterSystemInterpolationMethod.Unity) ? RigidbodyInterpolation.Interpolate : RigidbodyInterpolation.None;
                rigidbody.interpolation = interpMethod;
            }
        }
        public bool AutoSimulation = true;
        private float _lastCustomInterpolationStartTime = -1f;
        private float _lastCustomInterpolationDeltaTime = -1f;
        private void FixedUpdate()
        {
            if (AutoSimulation)
            {
                float deltaTime = Time.deltaTime;

                //PRE SIMULATE
                if (InterpolationMethod == CharacterSystemInterpolationMethod.Custom)
                {
                    transform.SetPositionAndRotation(TransientPosition, TransientRotation);
                    rigidbody.position = TransientPosition;
                    rigidbody.rotation = TransientRotation;
                }
                InitialTickPosition = transform.position;
                InitialTickRotation = transform.rotation;

                //SIMULATE
                UpdatePhase1(deltaTime);
                UpdatePhase2(deltaTime);
                transform.SetPositionAndRotation(TransientPosition, TransientRotation);
                rigidbody.position = TransientPosition;
                rigidbody.rotation = TransientRotation;

                //POST SIMULATE
                Physics.SyncTransforms();

                if (InterpolationMethod == CharacterSystemInterpolationMethod.Custom)
                {
                    _lastCustomInterpolationStartTime = Time.time;
                    _lastCustomInterpolationDeltaTime = deltaTime;
                }

                rigidbody.position = InitialTickPosition;
                rigidbody.rotation = InitialTickRotation;
                rigidbody.MovePosition(TransientPosition);
                rigidbody.MoveRotation(TransientRotation);
            }
        }
        private void Update()
        {
            if (InterpolationMethod == CharacterSystemInterpolationMethod.Custom)
            {
                float interpolationFactor = Mathf.Clamp01((Time.time - _lastCustomInterpolationStartTime) / _lastCustomInterpolationDeltaTime);
                transform.SetPositionAndRotation(Vector3.Lerp(InitialTickPosition, TransientPosition, interpolationFactor),
                    Quaternion.Slerp(InitialTickRotation, TransientRotation, interpolationFactor));
            }
        }
        #endregion


        private void Reset()
        {
            ValidateData();
        }

        private void OnValidate()
        {
            ValidateData();
        }

        [ContextMenu("Remove Component")]
        private void HandleRemoveComponent()
        {
            Rigidbody tmpRigidbody = GetComponent<Rigidbody>();
            CapsuleCollider tmpCapsule = GetComponent<CapsuleCollider>();
            DestroyImmediate(this);
            DestroyImmediate(tmpRigidbody);
            DestroyImmediate(tmpCapsule);
        }

        public CapsuleCollider capsuleCollider;
        public void ValidateData()
        {
            capsuleCollider = GetComponent<CapsuleCollider>();
            rigidbody.centerOfMass = Vector3.zero;
            rigidbody.useGravity = false;
            rigidbody.drag = 0f;
            rigidbody.angularDrag = 0f;
            rigidbody.maxAngularVelocity = Mathf.Infinity;
            rigidbody.maxDepenetrationVelocity = Mathf.Infinity;
            rigidbody.collisionDetectionMode = CollisionDetectionMode.Discrete;
            rigidbody.isKinematic = true;
            rigidbody.constraints = RigidbodyConstraints.None;
            rigidbody.interpolation = InterpolationMethod == CharacterSystemInterpolationMethod.Unity ? RigidbodyInterpolation.Interpolate : RigidbodyInterpolation.None;

            CapsuleRadius = Mathf.Clamp(CapsuleRadius, 0f, CapsuleHeight * 0.5f);
            capsuleCollider.isTrigger = false;
            capsuleCollider.direction = 1;
            capsuleCollider.sharedMaterial = capsulePhysicsMaterial;
            SetCapsuleDimensions(CapsuleRadius, CapsuleHeight, CapsuleYOffset);

            MaxStepHeight = Mathf.Clamp(MaxStepHeight, 0f, Mathf.Infinity);
            MinRequiredStepDepth = Mathf.Clamp(MinRequiredStepDepth, 0f, CapsuleRadius);

            MaxStableDistanceFromLedge = Mathf.Clamp(MaxStableDistanceFromLedge, 0f, CapsuleRadius);

            transform.localScale = Vector3.one;

#if UNITY_EDITOR
            capsuleCollider.hideFlags = HideFlags.NotEditable;
            rigidbody.hideFlags = HideFlags.NotEditable;
            if (!Mathf.Approximately(transform.lossyScale.x, 1f) || !Mathf.Approximately(transform.lossyScale.y, 1f) || !Mathf.Approximately(transform.lossyScale.z, 1f))
            {
                Debug.LogError("Character's lossy scale is not (1,1,1). This is not allowed. Make sure the character's transform and all of its parents have a (1,1,1) scale.", this.gameObject);
            }
#endif
        }


        public void SetCapsuleCollisionsActivation(bool kinematicCapsuleActive) => rigidbody.detectCollisions = kinematicCapsuleActive;
        public void SetMovementCollisionsSolvingActivation(bool movementCollisionsSolvingActive) => _solveMovementCollisions = movementCollisionsSolvingActive;
        public void SetGroundSolvingActivation(bool stabilitySolvingActive) => _solveGrounding = stabilitySolvingActive;

        public void SetPosition(Vector3 position, bool bypassInterpolation = true)
        {
            rigidbody.interpolation = RigidbodyInterpolation.None;
            transform.position = position;
            rigidbody.position = position;
            InitialSimulationPosition = position;
            TransientPosition = position;

            if (bypassInterpolation) InitialTickPosition = position;
            rigidbody.interpolation = InterpolationMethod == CharacterSystemInterpolationMethod.Unity ? RigidbodyInterpolation.Interpolate : RigidbodyInterpolation.None;
        }


        public void SetRotation(Quaternion rotation, bool bypassInterpolation = true)
        {
            rigidbody.interpolation = RigidbodyInterpolation.None;
            transform.rotation = rotation;
            rigidbody.rotation = rotation;
            InitialSimulationRotation = rotation;
            TransientRotation = rotation;

            if (bypassInterpolation) InitialTickRotation = rotation;
            rigidbody.interpolation = InterpolationMethod == CharacterSystemInterpolationMethod.Unity ? RigidbodyInterpolation.Interpolate : RigidbodyInterpolation.None;
        }


        public void SetPositionAndRotation(Vector3 position, Quaternion rotation, bool bypassInterpolation = true)
        {
            rigidbody.interpolation = RigidbodyInterpolation.None;
            transform.SetPositionAndRotation(position, rotation);
            rigidbody.position = position;
            rigidbody.rotation = rotation;
            InitialSimulationPosition = position;
            InitialSimulationRotation = rotation;
            TransientPosition = position;
            TransientRotation = rotation;

            if (bypassInterpolation)
            {
                InitialTickPosition = position;
                InitialTickRotation = rotation;
            }

            rigidbody.interpolation = InterpolationMethod == CharacterSystemInterpolationMethod.Unity ? RigidbodyInterpolation.Interpolate : RigidbodyInterpolation.None;
        }


        public void MoveCharacter(Vector3 toPosition)
        {
            _movePositionDirty = true;
            _movePositionTarget = toPosition;
        }


        public void RotateCharacter(Quaternion toRotation)
        {
            _moveRotationDirty = true;
            _moveRotationTarget = toRotation;
        }


        public CharacterMotorState GetState()
        {
            CharacterMotorState state = new CharacterMotorState();

            state.Position = TransientPosition;
            state.Rotation = TransientRotation;

            state.BaseVelocity = BaseVelocity;
            state.AttachedRigidbodyVelocity = AttachedRigidbodyVelocity;

            state.MustUnground = MustUnground;
            state.LastMovementIterationFoundAnyGround = LastMovementIterationFoundAnyGround;
            state.GroundingStatus.CopyFrom(GroundingStatus);
            state.AttachedRigidbody = AttachedRigidbody;

            return state;
        }


        public void ApplyState(CharacterMotorState state, bool bypassInterpolation = true)
        {
            SetPositionAndRotation(state.Position, state.Rotation, bypassInterpolation);

            BaseVelocity = state.BaseVelocity;
            AttachedRigidbodyVelocity = state.AttachedRigidbodyVelocity;

            MustUnground = state.MustUnground;
            LastMovementIterationFoundAnyGround = state.LastMovementIterationFoundAnyGround;
            GroundingStatus.CopyFrom(state.GroundingStatus);
            AttachedRigidbody = state.AttachedRigidbody;
        }


        public void SetCapsuleDimensions(float radius, float height, float yOffset)
        {
            CapsuleRadius = radius;
            CapsuleHeight = height;
            CapsuleYOffset = yOffset;

            capsuleCollider.radius = CapsuleRadius;
            capsuleCollider.height = Mathf.Clamp(CapsuleHeight, CapsuleRadius * 2f, CapsuleHeight);
            capsuleCollider.center = new Vector3(0f, CapsuleYOffset, 0f);

            CharacterTransformToCapsuleCenter = capsuleCollider.center;
            CharacterTransformToCapsuleBottom = capsuleCollider.center + (-_cachedWorldUp * (capsuleCollider.height * 0.5f));
            CharacterTransformToCapsuleTop = capsuleCollider.center + (_cachedWorldUp * (capsuleCollider.height * 0.5f));
            CharacterTransformToCapsuleBottomHemi = capsuleCollider.center + (-_cachedWorldUp * (capsuleCollider.height * 0.5f)) + (_cachedWorldUp * capsuleCollider.radius);
            CharacterTransformToCapsuleTopHemi = capsuleCollider.center + (_cachedWorldUp * (capsuleCollider.height * 0.5f)) + (-_cachedWorldUp * capsuleCollider.radius);
        }

        private void Awake()
        {
            ValidateData();

            TransientPosition = transform.position;
            TransientRotation = transform.rotation;

            CollidableLayers = 0;
            for (int i = 0; i < 32; i++)
                if (!Physics.GetIgnoreLayerCollision(gameObject.layer, i))
                    CollidableLayers |= (1 << i);

            SetCapsuleDimensions(CapsuleRadius, CapsuleHeight, CapsuleYOffset);
        }

        /// <summary>
        /// Update phase 1 is meant to be called after physics movers have calculated their velocities, but
        /// before they have simulated their goal positions/rotations. It is responsible for:
        /// - Initializing all values for update
        /// - Handling MovePosition calls
        /// - Solving initial collision overlaps
        /// - Ground probing
        /// - Handle detecting potential interactable rigidbodies
        /// </summary>
        public void UpdatePhase1(float deltaTime)
        {
            // NaN propagation safety stop
            if (float.IsNaN(BaseVelocity.x) || float.IsNaN(BaseVelocity.y) || float.IsNaN(BaseVelocity.z)) BaseVelocity = Vector3.zero;
            if (float.IsNaN(AttachedRigidbodyVelocity.x) || float.IsNaN(AttachedRigidbodyVelocity.y) || float.IsNaN(AttachedRigidbodyVelocity.z)) AttachedRigidbodyVelocity = Vector3.zero;

#if UNITY_EDITOR
            if (!Mathf.Approximately(transform.lossyScale.x, 1f) || !Mathf.Approximately(transform.lossyScale.y, 1f) || !Mathf.Approximately(transform.lossyScale.z, 1f))
            {
                Debug.LogError("Character's lossy scale is not (1,1,1). This is not allowed. Make sure the character's transform and all of its parents have a (1,1,1) scale.", this.gameObject);
            }
#endif

            // Before update
            BeforeCharacterUpdate(deltaTime);

            TransientPosition = transform.position;
            TransientRotation = transform.rotation;
            InitialSimulationPosition = TransientPosition;
            InitialSimulationRotation = TransientRotation;
            _rigidbodyProjectionHitCount = 0;
            OverlapsCount = 0;

            #region Handle Move Position
            if (_movePositionDirty)
            {
                if (_solveMovementCollisions)
                {
                    if (InternalCharacterMove((_movePositionTarget - TransientPosition), deltaTime, out _internalResultingMovementMagnitude, out _internalResultingMovementDirection))
                    {
                        if (InteractiveRigidbodyHandling)
                        {
                            Vector3 tmpVelocity = Vector3.zero;
                            ProcessVelocityForRigidbodyHits(ref tmpVelocity, deltaTime);
                        }
                    }
                }
                else
                {
                    TransientPosition = _movePositionTarget;
                }

                _movePositionDirty = false;
            }
            #endregion

            LastGroundingStatus.CopyFrom(GroundingStatus);
            GroundingStatus = new GroundingReport();
            GroundingStatus.GroundNormal = CharacterUp;

            if (_solveMovementCollisions)
            {
                #region Resolve initial overlaps
                Vector3 resolutionDirection = _cachedWorldUp;
                float resolutionDistance = 0f;
                int iterationsMade = 0;
                bool overlapSolved = false;
                while (iterationsMade < MaxDiscreteCollisionIterations && !overlapSolved)
                {
                    int nbOverlaps = CharacterCollisionsOverlap(TransientPosition, TransientRotation, _internalProbedColliders);
                    if (nbOverlaps > 0)
                    {
                        // Solve overlaps that aren't against dynamic rigidbodies or physics movers
                        for (int i = 0; i < nbOverlaps; i++)
                        {
                            Rigidbody probedRigidbody = _internalProbedColliders[i].attachedRigidbody;
                            bool isPhysicsMoverOrDynamicRigidbody = probedRigidbody && !probedRigidbody.isKinematic;
                            if (!isPhysicsMoverOrDynamicRigidbody)
                            {

                                // Process overlap
                                Transform overlappedTransform = _internalProbedColliders[i].GetComponent<Transform>();
                                if (Physics.ComputePenetration(capsuleCollider, TransientPosition, TransientRotation, _internalProbedColliders[i],
                                        overlappedTransform.position, overlappedTransform.rotation, out resolutionDirection, out resolutionDistance))
                                {
                                    // Resolve along obstruction direction
                                    Vector3 originalResolutionDirection = resolutionDirection;
                                    HitStabilityReport mockReport = new HitStabilityReport();
                                    mockReport.IsStable = IsStableOnNormal(resolutionDirection);
                                    resolutionDirection = GetObstructionNormal(resolutionDirection, mockReport);

                                    // Solve overlap
                                    Vector3 resolutionMovement = resolutionDirection * (resolutionDistance + CollisionOffset);
                                    TransientPosition += resolutionMovement;

                                    // Remember overlaps
                                    if (OverlapsCount < Overlaps.Length)
                                    {
                                        Overlaps[OverlapsCount] = new OverlapResult(resolutionDirection, _internalProbedColliders[i]);
                                        OverlapsCount++;
                                    }

                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        overlapSolved = true;
                    }

                    iterationsMade++;
                }
                #endregion
            }

            #region Ground Probing and Snapping
            // Handle ungrounding
            if (_solveGrounding)
            {
                if (MustUnground)
                {
                    TransientPosition += CharacterUp * (MinimumGroundProbingDistance * 1.5f);
                }
                else
                {
                    // Choose the appropriate ground probing distance
                    float selectedGroundProbingDistance = MinimumGroundProbingDistance;
                    if (!LastGroundingStatus.SnappingPrevented && (LastGroundingStatus.IsStableOnGround || LastMovementIterationFoundAnyGround))
                    {
                        if (StepHandling != StepHandlingMethod.None)
                        {
                            selectedGroundProbingDistance = Mathf.Max(CapsuleRadius, MaxStepHeight);
                        }
                        else
                        {
                            selectedGroundProbingDistance = CapsuleRadius;
                        }

                        selectedGroundProbingDistance += GroundDetectionExtraDistance;
                    }

                    ProbeGround(ref _internalTransientPosition, TransientRotation, selectedGroundProbingDistance, ref GroundingStatus);
                }
            }

            LastMovementIterationFoundAnyGround = false;
            MustUnground = false;
            #endregion

            if (_solveGrounding) PostGroundingUpdate(deltaTime);

            if (InteractiveRigidbodyHandling)
            {
                #region Interactive Rigidbody Handling 
                _lastAttachedRigidbody = AttachedRigidbody;
                if (AttachedRigidbodyOverride) AttachedRigidbody = AttachedRigidbodyOverride;
                else
                {
                    // Detect interactive rigidbodies from grounding
                    if (GroundingStatus.IsStableOnGround && GroundingStatus.GroundCollider.attachedRigidbody)
                    {
                        Rigidbody interactiveRigidbody = GetInteractiveRigidbody(GroundingStatus.GroundCollider);
                        if (interactiveRigidbody) AttachedRigidbody = interactiveRigidbody;
                    }
                    else AttachedRigidbody = null;
                }

                Vector3 tmpVelocityFromCurrentAttachedRigidbody = Vector3.zero;
                if (AttachedRigidbody)
                    tmpVelocityFromCurrentAttachedRigidbody = GetVelocityFromRigidbodyMovement(AttachedRigidbody, TransientPosition, deltaTime);

                // Conserve momentum when de-stabilized from an attached rigidbody
                if (PreserveAttachedRigidbodyMomentum && _lastAttachedRigidbody != null && AttachedRigidbody != _lastAttachedRigidbody)
                {
                    BaseVelocity += AttachedRigidbodyVelocity;
                    BaseVelocity -= tmpVelocityFromCurrentAttachedRigidbody;
                }

                // Process additionnal Velocity from attached rigidbody
                AttachedRigidbodyVelocity = _cachedZeroVector;
                if (AttachedRigidbody)
                {
                    AttachedRigidbodyVelocity = tmpVelocityFromCurrentAttachedRigidbody;

                    // Rotation from attached rigidbody
                    Vector3 newForward = Vector3.ProjectOnPlane(Quaternion.Euler(Mathf.Rad2Deg * AttachedRigidbody.angularVelocity * deltaTime) * CharacterForward, CharacterUp).normalized;
                    TransientRotation = Quaternion.LookRotation(newForward, CharacterUp);
                }

                // Cancel out horizontal velocity upon landing on an attached rigidbody
                if (GroundingStatus.GroundCollider && GroundingStatus.GroundCollider.attachedRigidbody &&
                     GroundingStatus.GroundCollider.attachedRigidbody == AttachedRigidbody &&
                     AttachedRigidbody != null && _lastAttachedRigidbody == null)
                {
                    BaseVelocity -= Vector3.ProjectOnPlane(AttachedRigidbodyVelocity, CharacterUp);
                }

                // Movement from Attached Rigidbody
                if (AttachedRigidbodyVelocity.sqrMagnitude > 0f)
                {
                    _isMovingFromAttachedRigidbody = true;

                    if (_solveMovementCollisions)
                    {
                        // Perform the move from rgdbdy velocity
                        if (InternalCharacterMove(AttachedRigidbodyVelocity * deltaTime, deltaTime, out _internalResultingMovementMagnitude, out _internalResultingMovementDirection))
                        {
                            AttachedRigidbodyVelocity = (_internalResultingMovementDirection * _internalResultingMovementMagnitude) / deltaTime;
                        }
                        else AttachedRigidbodyVelocity = Vector3.zero;
                    }
                    else TransientPosition += AttachedRigidbodyVelocity * deltaTime;

                    _isMovingFromAttachedRigidbody = false;
                }

                #endregion
            }
        }


        /// <summary>
        /// Update phase 2 is meant to be called after physics movers have simulated their goal positions/rotations. 
        /// At the end of this, the TransientPosition/Rotation values will be up-to-date with where the motor should be at the end of its move. 
        /// It is responsible for:
        /// - Solving Rotation
        /// - Handle MoveRotation calls
        /// - Solving potential attached rigidbody overlaps
        /// - Solving Velocity
        /// - Applying planar constraint
        /// </summary>

        public void UpdatePhase2(float deltaTime)
        {
            // Handle rotation
            UpdateRotation(ref _internalTransientRotation, deltaTime);
            TransientRotation = _internalTransientRotation;

            // Handle move rotation
            if (_moveRotationDirty)
            {
                TransientRotation = _moveRotationTarget;
                _moveRotationDirty = false;
            }

            if (_solveMovementCollisions && InteractiveRigidbodyHandling)
            {
                if (InteractiveRigidbodyHandling)
                {
                    if (AttachedRigidbody)
                    {
                        float upwardsOffset = capsuleCollider.radius;

                        RaycastHit closestHit;
                        if (CharacterGroundSweep(TransientPosition + (CharacterUp * upwardsOffset),
                            TransientRotation, -CharacterUp, upwardsOffset, out closestHit))
                        {
                            if (closestHit.collider.attachedRigidbody == AttachedRigidbody && IsStableOnNormal(closestHit.normal))
                            {
                                float distanceMovedUp = (upwardsOffset - closestHit.distance);
                                TransientPosition = TransientPosition + (CharacterUp * distanceMovedUp) + (CharacterUp * CollisionOffset);
                            }
                        }
                    }
                }

                if (SafeMovement || InteractiveRigidbodyHandling)
                {
                    /*    Vector3 resolutionDirection = _cachedWorldUp;
                        float resolutionDistance = 0f;
                        int iterationsMade = 0;
                        bool overlapSolved = false;

                        while (iterationsMade < MaxDiscreteCollisionIterations && !overlapSolved)
                        {
                            int nbOverlaps = CharacterCollisionsOverlap(TransientPosition, TransientRotation, _internalProbedColliders);
                            if (nbOverlaps > 0) //> 0
                            {
                                for (int i = 0; i < nbOverlaps; i++)// = 0
                                {
                                    // Process overlap
                                    Transform overlappedTransform = _internalProbedColliders[i].GetComponent<Transform>();
                                    if (Physics.ComputePenetration(capsuleCollider, TransientPosition, TransientRotation, _internalProbedColliders[i],
                                            overlappedTransform.position, overlappedTransform.rotation, out resolutionDirection, out resolutionDistance))
                                    {
                                        // Resolve along obstruction direction
                                        Vector3 originalResolutionDirection = resolutionDirection;
                                        HitStabilityReport mockReport = new HitStabilityReport();
                                        mockReport.IsStable = IsStableOnNormal(resolutionDirection);
                                        resolutionDirection = GetObstructionNormal(resolutionDirection, mockReport);

                                        // Solve overlap
                                        Vector3 resolutionMovement = resolutionDirection * (resolutionDistance + CollisionOffset);
                                        TransientPosition += resolutionMovement;

                                        // If physicsMover, register as rigidbody hit for velocity
                                        if (InteractiveRigidbodyHandling)
                                        {
                                            Rigidbody probedRigidbody = _internalProbedColliders[i].attachedRigidbody;
                                            if (probedRigidbody)
                                            {
                                                //PhysicsMover physicsMover = probedRigidbody.GetComponent<PhysicsMover>();
                                                Rigidbody physicsMover = probedRigidbody.GetComponent<Rigidbody>();
                                                if (physicsMover)
                                                {
                                                    bool isPhysicsMoverOrDynamicRigidbody = probedRigidbody && (!probedRigidbody.isKinematic || physicsMover);
                                                    if (isPhysicsMoverOrDynamicRigidbody)
                                                    {
                                                        HitStabilityReport tmpReport = new HitStabilityReport();
                                                        tmpReport.IsStable = IsStableOnNormal(resolutionDirection);
                                                        if (tmpReport.IsStable)
                                                        {
                                                            LastMovementIterationFoundAnyGround = tmpReport.IsStable;
                                                        }
                                                        if (physicsMover && physicsMover != AttachedRigidbody)
                                                        {
                                                            Vector3 characterCenter = TransientPosition + (TransientRotation * CharacterTransformToCapsuleCenter);
                                                            Vector3 estimatedCollisionPoint = TransientPosition;

                                                            MeshCollider meshColl = _internalProbedColliders[i] as MeshCollider;
                                                            if (!(meshColl && !meshColl.convex))
                                                            {
                                                                Physics.ClosestPoint(characterCenter, _internalProbedColliders[i], overlappedTransform.position, overlappedTransform.rotation);
                                                            }

                                                            StoreRigidbodyHit(physicsMover, Velocity, estimatedCollisionPoint,
                                                                resolutionDirection, tmpReport);
                                                        }
                                                    }
                                                }

                                            }
                                        }

                                        // Remember overlaps
                                        if (OverlapsCount < Overlaps.Length)
                                        {
                                            Overlaps[OverlapsCount] = new OverlapResult(resolutionDirection, _internalProbedColliders[i]);
                                            OverlapsCount++;
                                        }

                                        break;
                                    }
                                }
                            }
                            else
                            {
                                overlapSolved = true;
                            }

                            iterationsMade++;
                        }
                    */
                }
            }

            // Handle velocity
            UpdateVelocity(ref _baseVelocity, deltaTime);
            if (BaseVelocity.magnitude < MinVelocityMagnitude) BaseVelocity = Vector3.zero;

            #region Calculate Character movement from base velocity   
            // Perform the move from base velocity
            if (BaseVelocity.sqrMagnitude > 0f)
            {
                if (_solveMovementCollisions)
                {
                    if (InternalCharacterMove(BaseVelocity * deltaTime, deltaTime, out _internalResultingMovementMagnitude, out _internalResultingMovementDirection))
                        BaseVelocity = (_internalResultingMovementDirection * _internalResultingMovementMagnitude) / deltaTime;
                    else BaseVelocity = Vector3.zero;
                }
                else TransientPosition += BaseVelocity * deltaTime;
            }

            // Process rigidbody hits/overlaps to affect velocity
            if (InteractiveRigidbodyHandling) ProcessVelocityForRigidbodyHits(ref _baseVelocity, deltaTime);
            #endregion

            // Handle planar constraint
            if (HasPlanarConstraint) TransientPosition = InitialSimulationPosition + Vector3.ProjectOnPlane(TransientPosition - InitialSimulationPosition, PlanarConstraintAxis.normalized);

            // Discrete collision detection
            if (DetectDiscreteCollisions)
            {
                int nbOverlaps = CharacterCollisionsOverlap(TransientPosition, TransientRotation, _internalProbedColliders, CollisionOffset * 2f);
                for (int i = 0; i < nbOverlaps; i++)
                    OnDiscreteCollisionDetected(_internalProbedColliders[i]);
            }

            AfterCharacterUpdate(deltaTime);
        }


        public void ForceUnground() => MustUnground = true;
        private bool IsStableOnNormal(Vector3 normal) => Vector3.Angle(CharacterUp, normal) <= MaxStableSlopeAngle;

        public void ProbeGround(ref Vector3 probingPosition, Quaternion atRotation, float probingDistance, ref GroundingReport groundingReport)
        {
            if (probingDistance < MinimumGroundProbingDistance)
            {
                probingDistance = MinimumGroundProbingDistance;
            }

            int groundSweepsMade = 0;
            RaycastHit groundSweepHit = new RaycastHit();
            bool groundSweepingIsOver = false;
            Vector3 groundSweepPosition = probingPosition;
            Vector3 groundSweepDirection = (atRotation * -_cachedWorldUp);
            float groundProbeDistanceRemaining = probingDistance;
            while (groundProbeDistanceRemaining > 0 && (groundSweepsMade <= MaxGroundingSweepIterations) && !groundSweepingIsOver)
            {
                // Sweep for ground detection
                if (CharacterGroundSweep(
                        groundSweepPosition, // position
                        atRotation, // rotation
                        groundSweepDirection, // direction
                        groundProbeDistanceRemaining, // distance
                        out groundSweepHit)) // hit
                {
                    Vector3 targetPosition = groundSweepPosition + (groundSweepDirection * groundSweepHit.distance);
                    HitStabilityReport groundHitStabilityReport = new HitStabilityReport();
                    EvaluateHitStability(groundSweepHit.collider, groundSweepHit.normal, groundSweepHit.point, targetPosition, TransientRotation, ref groundHitStabilityReport);

                    // Handle ledge stability
                    if (groundHitStabilityReport.LedgeDetected)
                    {
                        if (groundHitStabilityReport.IsOnEmptySideOfLedge && groundHitStabilityReport.DistanceFromLedge > MaxStableDistanceFromLedge)
                        {
                            groundHitStabilityReport.IsStable = false;
                        }
                    }

                    groundingReport.FoundAnyGround = true;
                    groundingReport.GroundNormal = groundSweepHit.normal;
                    groundingReport.InnerGroundNormal = groundHitStabilityReport.InnerNormal;
                    groundingReport.OuterGroundNormal = groundHitStabilityReport.OuterNormal;
                    groundingReport.GroundCollider = groundSweepHit.collider;
                    groundingReport.GroundPoint = groundSweepHit.point;
                    groundingReport.SnappingPrevented = false;

                    // Found stable ground
                    if (groundHitStabilityReport.IsStable)
                    {
                        // Find all scenarios where ground snapping should be canceled
                        if (LedgeHandling)
                        {
                            // "Launching" off of slopes of a certain denivelation angle
                            if (LastGroundingStatus.FoundAnyGround && groundHitStabilityReport.InnerNormal.sqrMagnitude != 0f && groundHitStabilityReport.OuterNormal.sqrMagnitude != 0f)
                            {
                                float denivelationAngle = Vector3.Angle(groundHitStabilityReport.InnerNormal, groundHitStabilityReport.OuterNormal);
                                if (denivelationAngle > MaxStableDenivelationAngle)
                                {
                                    groundingReport.SnappingPrevented = true;
                                }
                                else
                                {
                                    denivelationAngle = Vector3.Angle(LastGroundingStatus.InnerGroundNormal, groundHitStabilityReport.OuterNormal);
                                    if (denivelationAngle > MaxStableDenivelationAngle)
                                    {
                                        groundingReport.SnappingPrevented = true;
                                    }
                                }
                            }

                            // Ledge stability
                            if (PreventSnappingOnLedges && groundHitStabilityReport.LedgeDetected)
                            {
                                groundingReport.SnappingPrevented = true;
                            }
                        }

                        groundingReport.IsStableOnGround = true;

                        // Ground snapping
                        if (!groundingReport.SnappingPrevented)
                        {
                            targetPosition += (-groundSweepDirection * CollisionOffset);
                            InternalMoveCharacterPosition(ref probingPosition, targetPosition, atRotation);
                        }

                        OnGroundHit(groundSweepHit.collider, groundSweepHit.normal, groundSweepHit.point, ref groundHitStabilityReport);
                        groundSweepingIsOver = true;
                    }
                    else
                    {
                        // Calculate movement from this iteration and advance position
                        Vector3 sweepMovement = (groundSweepDirection * groundSweepHit.distance) + ((atRotation * Vector3.up) * Mathf.Clamp(CollisionOffset, 0f, groundSweepHit.distance));
                        groundSweepPosition = groundSweepPosition + sweepMovement;

                        // Set remaining distance
                        groundProbeDistanceRemaining = Mathf.Min(GroundProbeReboundDistance, Mathf.Clamp(groundProbeDistanceRemaining - sweepMovement.magnitude, 0f, Mathf.Infinity));

                        // Reorient direction
                        groundSweepDirection = Vector3.ProjectOnPlane(groundSweepDirection, groundSweepHit.normal).normalized;
                    }
                }
                else
                {
                    groundSweepingIsOver = true;
                }

                groundSweepsMade++;
            }
        }


        public Vector3 GetDirectionTangentToSurface(Vector3 direction, Vector3 surfaceNormal)
        {
            Vector3 directionRight = Vector3.Cross(direction, CharacterUp);
            return Vector3.Cross(surfaceNormal, directionRight).normalized;
        }
        private bool InternalCharacterMove(Vector3 movement, float deltaTime, out float resultingMovementMagnitude, out Vector3 resultingMovementDirection)
        {
            _rigidbodiesPushedCount = 0;
            bool wasCompleted = true;
            Vector3 remainingMovementDirection = movement.normalized;
            float remainingMovementMagnitude = movement.magnitude;
            resultingMovementDirection = remainingMovementDirection;
            resultingMovementMagnitude = remainingMovementMagnitude;
            int sweepsMade = 0;
            RaycastHit closestSweepHit;
            bool hitSomethingThisSweepIteration = true;
            Vector3 tmpMovedPosition = TransientPosition;
            Vector3 targetPositionAfterSweep = TransientPosition;
            Vector3 originalMoveDirection = movement.normalized;
            Vector3 previousMovementHitNormal = _cachedZeroVector;
            MovementSweepState sweepState = MovementSweepState.Initial;

            // Project movement against current overlaps
            for (int i = 0; i < OverlapsCount; i++)
            {
                if (Vector3.Dot(remainingMovementDirection, Overlaps[i].Normal) < 0f)
                {
                    InternalHandleMovementProjection(
                        IsStableOnNormal(
                            Overlaps[i].Normal) && !MustUnground,
                            Overlaps[i].Normal,
                            Overlaps[i].Normal,
                            originalMoveDirection,
                            ref sweepState,
                            ref previousMovementHitNormal,
                            ref resultingMovementMagnitude,
                            ref remainingMovementDirection,
                            ref remainingMovementMagnitude);
                }
            }

            // Sweep the desired movement to detect collisions
            while (remainingMovementMagnitude > 0f &&
                (sweepsMade <= MaxMovementSweepIterations) &&
                hitSomethingThisSweepIteration)
            {
                if (CharacterCollisionsSweep(
                        tmpMovedPosition, // position
                        TransientRotation, // rotation
                        remainingMovementDirection, // direction
                        remainingMovementMagnitude + CollisionOffset, // distance
                        out closestSweepHit, // closest hit
                        _internalCharacterHits) // all hits
                    > 0)
                {
                    // Calculate movement from this iteration
                    targetPositionAfterSweep = tmpMovedPosition + (remainingMovementDirection * closestSweepHit.distance) + (closestSweepHit.normal * CollisionOffset);
                    Vector3 sweepMovement = targetPositionAfterSweep - tmpMovedPosition;

                    // Evaluate if hit is stable
                    HitStabilityReport moveHitStabilityReport = new HitStabilityReport();
                    EvaluateHitStability(closestSweepHit.collider, closestSweepHit.normal, closestSweepHit.point, targetPositionAfterSweep, TransientRotation, ref moveHitStabilityReport);

                    // Handle stepping up perfectly vertical walls
                    bool foundValidStepHit = false;
                    if (_solveGrounding && StepHandling != StepHandlingMethod.None && moveHitStabilityReport.ValidStepDetected)
                    {
                        float obstructionCorrelation = Mathf.Abs(Vector3.Dot(closestSweepHit.normal, CharacterUp));
                        if (obstructionCorrelation <= CorrelationForVerticalObstruction)
                        {
                            RaycastHit closestStepHit;
                            Vector3 stepForwardDirection = Vector3.ProjectOnPlane(-closestSweepHit.normal, CharacterUp).normalized;
                            Vector3 stepCastStartPoint = (targetPositionAfterSweep + (stepForwardDirection * SteppingForwardDistance)) +
                                (CharacterUp * MaxStepHeight);

                            // Cast downward from the top of the stepping height
                            int nbStepHits = CharacterCollisionsSweep(
                                                stepCastStartPoint, // position
                                                TransientRotation, // rotation
                                                -CharacterUp, // direction
                                                MaxStepHeight, // distance
                                                out closestStepHit, // closest hit
                                                _internalCharacterHits); // all hitswwasa  

                            // Check for hit corresponding to stepped collider
                            for (int i = 0; i < nbStepHits; i++)
                            {
                                if (_internalCharacterHits[i].collider == moveHitStabilityReport.SteppedCollider)
                                {

                                    Vector3 endStepPosition = stepCastStartPoint + (-CharacterUp * (_internalCharacterHits[i].distance - CollisionOffset));
                                    tmpMovedPosition = endStepPosition;
                                    foundValidStepHit = true;

                                    // Consume magnitude for step
                                    remainingMovementMagnitude = Mathf.Clamp(remainingMovementMagnitude - sweepMovement.magnitude, 0f, Mathf.Infinity);
                                    break;
                                }
                            }
                        }
                    }

                    // Handle movement solving
                    if (!foundValidStepHit)
                    {
                        // Apply the actual movement
                        tmpMovedPosition = targetPositionAfterSweep;
                        remainingMovementMagnitude = Mathf.Clamp(remainingMovementMagnitude - sweepMovement.magnitude, 0f, Mathf.Infinity);

                        // Movement hit callback
                        OnMovementHit(closestSweepHit.collider, closestSweepHit.normal, closestSweepHit.point, ref moveHitStabilityReport);
                        Vector3 obstructionNormal = GetObstructionNormal(closestSweepHit.normal, moveHitStabilityReport);

                        // Handle remembering rigidbody hits
                        if (InteractiveRigidbodyHandling && closestSweepHit.collider.attachedRigidbody)
                        {
                            StoreRigidbodyHit(
                                closestSweepHit.collider.attachedRigidbody,
                                (remainingMovementDirection * resultingMovementMagnitude) / deltaTime,
                                closestSweepHit.point,
                                obstructionNormal,
                                moveHitStabilityReport);
                        }

                        // Project movement
                        InternalHandleMovementProjection(
                            moveHitStabilityReport.IsStable && !MustUnground,
                            closestSweepHit.normal,
                            obstructionNormal,
                            originalMoveDirection,
                            ref sweepState,
                            ref previousMovementHitNormal,
                            ref resultingMovementMagnitude,
                            ref remainingMovementDirection,
                            ref remainingMovementMagnitude);
                    }
                }
                // If we hit nothing...
                else
                {
                    hitSomethingThisSweepIteration = false;
                }

                // Safety for exceeding max sweeps allowed
                sweepsMade++;
                if (sweepsMade > MaxMovementSweepIterations)
                {
                    remainingMovementMagnitude = 0;
                    wasCompleted = false;
                }
            }

            // Move position for the remainder of the movement
            Vector3 targetFinalPosition = tmpMovedPosition + (remainingMovementDirection * remainingMovementMagnitude);
            InternalMoveCharacterPosition(ref _internalTransientPosition, targetFinalPosition, TransientRotation);
            resultingMovementDirection = remainingMovementDirection;

            return wasCompleted;
        }


        private Vector3 GetObstructionNormal(Vector3 hitNormal, HitStabilityReport hitStabilityReport)
        {
            // Find hit/obstruction/offset normal
            Vector3 obstructionNormal = hitNormal;
            if (GroundingStatus.IsStableOnGround && !MustUnground && !hitStabilityReport.IsStable)
            {
                Vector3 obstructionLeftAlongGround = Vector3.Cross(GroundingStatus.GroundNormal, obstructionNormal).normalized;
                obstructionNormal = Vector3.Cross(obstructionLeftAlongGround, CharacterUp).normalized;
            }

            // Catch cases where cross product between parallel normals returned 0
            if (obstructionNormal == Vector3.zero) obstructionNormal = hitNormal;
            return obstructionNormal;
        }
        private void StoreRigidbodyHit(Rigidbody hitRigidbody, Vector3 hitVelocity, Vector3 hitPoint, Vector3 obstructionNormal, HitStabilityReport hitStabilityReport)
        {
            if (_rigidbodyProjectionHitCount < _internalRigidbodyProjectionHits.Length)
            {
                if (!hitRigidbody.GetComponent<CharacterMotor>())
                {
                    RigidbodyProjectionHit rph = new RigidbodyProjectionHit();
                    rph.Rigidbody = hitRigidbody;
                    rph.HitPoint = hitPoint;
                    rph.EffectiveHitNormal = obstructionNormal;
                    rph.HitVelocity = hitVelocity;
                    rph.StableOnHit = hitStabilityReport.IsStable;

                    _internalRigidbodyProjectionHits[_rigidbodyProjectionHitCount] = rph;
                    _rigidbodyProjectionHitCount++;
                }
            }
        }
        private void InternalHandleMovementProjection(bool stableOnHit, Vector3 hitNormal, Vector3 obstructionNormal, Vector3 originalMoveDirection, ref MovementSweepState sweepState,
            ref Vector3 previousObstructionNormal, ref float resultingMovementMagnitude, ref Vector3 remainingMovementDirection, ref float remainingMovementMagnitude)
        {
            if (remainingMovementMagnitude <= 0)
            {
                return;
            }

            Vector3 remainingMovement = originalMoveDirection * remainingMovementMagnitude;
            float remainingMagnitudeBeforeProj = remainingMovementMagnitude;
            if (stableOnHit)
            {
                LastMovementIterationFoundAnyGround = true;
            }

            // Blocking-corner handling
            if (sweepState == MovementSweepState.FoundBlockingCrease)
            {
                remainingMovementMagnitude = 0f;
                resultingMovementMagnitude = 0f;

                sweepState = MovementSweepState.FoundBlockingCorner;
            }
            // Handle projection
            else
            {
                HandleMovementProjection(ref remainingMovement, obstructionNormal, stableOnHit);

                remainingMovementDirection = remainingMovement.normalized;
                remainingMovementMagnitude = remainingMovement.magnitude;
                resultingMovementMagnitude = (remainingMovementMagnitude / remainingMagnitudeBeforeProj) * resultingMovementMagnitude;

                // Blocking corner handling
                if (sweepState == MovementSweepState.Initial)
                {
                    sweepState = MovementSweepState.AfterFirstHit;
                }
                else if (sweepState == MovementSweepState.AfterFirstHit)
                {
                    // Detect blocking corners
                    if (Vector3.Dot(previousObstructionNormal, remainingMovementDirection) < 0f)
                    {
                        Vector3 cornerVector = Vector3.Cross(previousObstructionNormal, obstructionNormal).normalized;
                        remainingMovement = Vector3.Project(remainingMovement, cornerVector);
                        remainingMovementDirection = remainingMovement.normalized;
                        remainingMovementMagnitude = remainingMovement.magnitude;
                        resultingMovementMagnitude = (remainingMovementMagnitude / remainingMagnitudeBeforeProj) * resultingMovementMagnitude;

                        sweepState = MovementSweepState.FoundBlockingCrease;
                    }
                }
            }

            previousObstructionNormal = obstructionNormal;
        }

        private bool InternalMoveCharacterPosition(ref Vector3 movedPosition, Vector3 targetPosition, Quaternion atRotation)
        {
            bool movementValid = true;
            if (SafeMovement)
            {
                int nbOverlaps = CharacterCollisionsOverlap(targetPosition, atRotation, _internalProbedColliders);
                if (nbOverlaps > 0)
                {
                    movementValid = false;
                }
            }

            if (movementValid)
            {
                movedPosition = targetPosition;
                return true;
            }
            return false;
        }
        private void ProcessVelocityForRigidbodyHits(ref Vector3 processedVelocity, float deltaTime)
        {
            for (int i = 0; i < _rigidbodyProjectionHitCount; i++)
            {
                if (_internalRigidbodyProjectionHits[i].Rigidbody)
                {
                    // Keep track of the unique rigidbodies we pushed this update, to avoid doubling their effect
                    bool alreadyPushedThisRigidbody = false;
                    for (int j = 0; j < _rigidbodiesPushedCount; j++)
                    {
                        if (_rigidbodiesPushedThisMove[j] == _internalRigidbodyProjectionHits[j].Rigidbody)
                        {
                            alreadyPushedThisRigidbody = true;
                            break;
                        }
                    }

                    if (!alreadyPushedThisRigidbody && _internalRigidbodyProjectionHits[i].Rigidbody != AttachedRigidbody)
                    {
                        if (_rigidbodiesPushedCount < _rigidbodiesPushedThisMove.Length)
                        {
                            // Remember we hit this rigidbody
                            _rigidbodiesPushedThisMove[_rigidbodiesPushedCount] = _internalRigidbodyProjectionHits[i].Rigidbody;
                            _rigidbodiesPushedCount++;

                            if (RigidbodyInteractionType == RigidbodyInteractionType.SimulatedDynamic)
                            {
                                HandleSimulatedRigidbodyInteraction(ref processedVelocity, _internalRigidbodyProjectionHits[i], deltaTime);
                            }
                        }
                    }
                }
            }
        }

        private bool CheckIfColliderValidForCollisions(Collider coll)
        {
            // Ignore self
            if (coll == null || coll == capsuleCollider) return false;
            if (!IsColliderValidForCollisions(coll)) return false;
            return true;
        }
        private bool IsColliderValidForCollisions(Collider coll)
        {
            // Ignore dynamic rigidbodies if the movement is made from AttachedRigidbody, or if RigidbodyInteractionType is kinematic
            if ((_isMovingFromAttachedRigidbody || RigidbodyInteractionType == RigidbodyInteractionType.Kinematic) && coll.attachedRigidbody && !coll.attachedRigidbody.isKinematic)
                return false;

            // If movement is made from AttachedRigidbody, ignore the AttachedRigidbody
            if (_isMovingFromAttachedRigidbody && coll.attachedRigidbody == AttachedRigidbody)
                return false;

            // Custom checks
            if (!IsColliderValidForCollisionsCustom(coll))
                return false;

            return true;
        }


        public void EvaluateHitStability(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, Vector3 atCharacterPosition, Quaternion atCharacterRotation, ref HitStabilityReport stabilityReport)
        {
            if (!_solveGrounding)
            {
                stabilityReport.IsStable = false;
                return;
            }

            bool isStableOnNormal = false;
            Vector3 atCharacterUp = atCharacterRotation * Vector3.up;
            Vector3 innerHitDirection = Vector3.ProjectOnPlane(hitNormal, atCharacterUp).normalized;

            isStableOnNormal = this.IsStableOnNormal(hitNormal);
            stabilityReport.InnerNormal = hitNormal;
            stabilityReport.OuterNormal = hitNormal;

            // Step handling
            if (StepHandling != StepHandlingMethod.None && !isStableOnNormal)
            {
                // Stepping not supported on dynamic rigidbodies
                Rigidbody hitRigidbody = hitCollider.attachedRigidbody;
                if (!(hitRigidbody && !hitRigidbody.isKinematic))
                {
                    DetectSteps(atCharacterPosition, atCharacterRotation, hitPoint, innerHitDirection, ref stabilityReport);
                }
            }

            // Ledge handling
            if (LedgeHandling)
            {
                float ledgeCheckHeight = MinDistanceForLedge;
                if (StepHandling != StepHandlingMethod.None)
                {
                    ledgeCheckHeight = MaxStepHeight;
                }

                bool isStableLedgeInner = false;
                bool isStableLedgeOuter = false;

                RaycastHit innerLedgeHit;
                if (CharacterCollisionsRaycast(
                    hitPoint + (atCharacterUp * SecondaryProbesVertical) + (innerHitDirection * SecondaryProbesHorizontal),
                    -atCharacterUp,
                    ledgeCheckHeight + SecondaryProbesVertical,
                    out innerLedgeHit,
                    _internalCharacterHits) > 0)
                {
                    stabilityReport.InnerNormal = innerLedgeHit.normal;
                    isStableLedgeInner = IsStableOnNormal(innerLedgeHit.normal);
                }

                RaycastHit outerLedgeHit;
                if (CharacterCollisionsRaycast(
                    hitPoint + (atCharacterUp * SecondaryProbesVertical) + (-innerHitDirection * SecondaryProbesHorizontal),
                    -atCharacterUp,
                    ledgeCheckHeight + SecondaryProbesVertical,
                    out outerLedgeHit,
                    _internalCharacterHits) > 0)
                {
                    stabilityReport.OuterNormal = outerLedgeHit.normal;
                    isStableLedgeOuter = IsStableOnNormal(outerLedgeHit.normal);
                }

                stabilityReport.LedgeDetected = (isStableLedgeInner != isStableLedgeOuter);
                if (stabilityReport.LedgeDetected)
                {
                    stabilityReport.IsOnEmptySideOfLedge = isStableLedgeOuter && !isStableLedgeInner;
                    stabilityReport.LedgeGroundNormal = isStableLedgeOuter ? outerLedgeHit.normal : innerLedgeHit.normal;
                    stabilityReport.LedgeRightDirection = Vector3.Cross(hitNormal, outerLedgeHit.normal).normalized;
                    stabilityReport.LedgeFacingDirection = Vector3.Cross(stabilityReport.LedgeGroundNormal, stabilityReport.LedgeRightDirection).normalized;
                    stabilityReport.DistanceFromLedge = Vector3.ProjectOnPlane((hitPoint - (atCharacterPosition + (atCharacterRotation * CharacterTransformToCapsuleBottom))), atCharacterUp).magnitude;
                }
            }

            // Final stability evaluation
            if (isStableOnNormal || stabilityReport.ValidStepDetected)
            {
                stabilityReport.IsStable = true;
            }

            ProcessHitStabilityReport(hitCollider, hitNormal, hitPoint, atCharacterPosition, atCharacterRotation, ref stabilityReport);
        }


        private void DetectSteps(Vector3 characterPosition, Quaternion characterRotation, Vector3 hitPoint, Vector3 innerHitDirection, ref HitStabilityReport stabilityReport)
        {
            int nbStepHits = 0;
            Collider tmpCollider;
            RaycastHit outerStepHit;
            Vector3 characterUp = characterRotation * Vector3.up;
            Vector3 stepCheckStartPos = characterPosition;

            // Do outer step check with capsule cast on hit point
            stepCheckStartPos = characterPosition + (characterUp * MaxStepHeight) + (-innerHitDirection * CapsuleRadius);
            nbStepHits = CharacterCollisionsSweep(
                        stepCheckStartPos,
                        characterRotation,
                        -characterUp,
                        MaxStepHeight - CollisionOffset,
                        out outerStepHit,
                        _internalCharacterHits);

            // Check for overlaps and obstructions at the hit position
            if (CheckStepValidity(nbStepHits, characterPosition, characterRotation, innerHitDirection, stepCheckStartPos, out tmpCollider))
            {
                stabilityReport.ValidStepDetected = true;
                stabilityReport.SteppedCollider = tmpCollider;
            }

            if (StepHandling == StepHandlingMethod.Extra && !stabilityReport.ValidStepDetected)
            {
                // Do min reach step check with capsule cast on hit point
                stepCheckStartPos = characterPosition + (characterUp * MaxStepHeight) + (-innerHitDirection * MinRequiredStepDepth);
                nbStepHits = CharacterCollisionsSweep(
                            stepCheckStartPos,
                            characterRotation,
                            -characterUp,
                            MaxStepHeight - CollisionOffset,
                            out outerStepHit,
                            _internalCharacterHits);

                // Check for overlaps and obstructions at the hit position
                if (CheckStepValidity(nbStepHits, characterPosition, characterRotation, innerHitDirection, stepCheckStartPos, out tmpCollider))
                {
                    stabilityReport.ValidStepDetected = true;
                    stabilityReport.SteppedCollider = tmpCollider;
                }
            }
        }
        private bool CheckStepValidity(int nbStepHits, Vector3 characterPosition, Quaternion characterRotation, Vector3 innerHitDirection, Vector3 stepCheckStartPos, out Collider hitCollider)
        {
            hitCollider = null;
            Vector3 characterUp = characterRotation * Vector3.up;

            // Find the farthest valid hit for stepping
            bool foundValidStepPosition = false;
            while (nbStepHits > 0 && !foundValidStepPosition)
            {
                // Get farthest hit among the remaining hits
                RaycastHit farthestHit = new RaycastHit();
                float farthestDistance = 0f;
                int farthestIndex = 0;
                for (int i = 0; i < nbStepHits; i++)
                {
                    if (_internalCharacterHits[i].distance > farthestDistance)
                    {
                        farthestDistance = _internalCharacterHits[i].distance;
                        farthestHit = _internalCharacterHits[i];
                        farthestIndex = i;
                    }
                }

                Vector3 characterBottom = characterPosition + (characterRotation * CharacterTransformToCapsuleBottom);
                float hitHeight = Vector3.Project(farthestHit.point - characterBottom, characterUp).magnitude;

                Vector3 characterPositionAtHit = stepCheckStartPos + (-characterUp * (farthestHit.distance - CollisionOffset));

                if (hitHeight <= MaxStepHeight)
                {
                    int atStepOverlaps = CharacterCollisionsOverlap(characterPositionAtHit, characterRotation, _internalProbedColliders);
                    if (atStepOverlaps <= 0)
                    {
                        // Check for outer hit slope normal stability at the step position
                        RaycastHit outerSlopeHit;
                        if (CharacterCollisionsRaycast(
                                farthestHit.point + (characterUp * SecondaryProbesVertical) + (-innerHitDirection * SecondaryProbesHorizontal),
                                -characterUp,
                                MaxStepHeight + SecondaryProbesVertical,
                                out outerSlopeHit,
                                _internalCharacterHits) > 0)
                        {
                            if (IsStableOnNormal(outerSlopeHit.normal))
                            {
                                // Cast upward to detect any obstructions to moving there
                                RaycastHit tmpUpObstructionHit;
                                if (CharacterCollisionsSweep(
                                                    characterPosition, // position
                                                    characterRotation, // rotation
                                                    characterUp, // direction
                                                    MaxStepHeight - farthestHit.distance, // distance
                                                    out tmpUpObstructionHit, // closest hit
                                                    _internalCharacterHits) // all hits
                                        <= 0)
                                {
                                    // Do inner step check...
                                    bool innerStepValid = false;
                                    RaycastHit innerStepHit;

                                    // At the capsule center at the step height
                                    if (CharacterCollisionsRaycast(
                                        characterPosition + Vector3.Project((characterPositionAtHit - characterPosition), characterUp),
                                        -characterUp,
                                        MaxStepHeight,
                                        out innerStepHit,
                                        _internalCharacterHits) > 0)
                                    {
                                        if (IsStableOnNormal(innerStepHit.normal))
                                        {
                                            innerStepValid = true;
                                        }
                                    }

                                    if (!innerStepValid)
                                    {
                                        // At inner step of the step point
                                        if (CharacterCollisionsRaycast(
                                            farthestHit.point + (innerHitDirection * SecondaryProbesHorizontal),
                                            -characterUp,
                                            MaxStepHeight,
                                            out innerStepHit,
                                            _internalCharacterHits) > 0)
                                        {
                                            if (IsStableOnNormal(innerStepHit.normal))
                                            {
                                                innerStepValid = true;
                                            }
                                        }
                                    }

                                    if (!innerStepValid)
                                    {
                                        // At the current ground point at the step height
                                    }

                                    // Final validation of step
                                    if (innerStepValid)
                                    {
                                        hitCollider = farthestHit.collider;
                                        foundValidStepPosition = true;
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }

                // Discard hit if not valid step
                if (!foundValidStepPosition)
                {
                    nbStepHits--;
                    if (farthestIndex < nbStepHits)
                    {
                        _internalCharacterHits[farthestIndex] = _internalCharacterHits[nbStepHits];
                    }
                }
            }

            return false;
        }
        public Vector3 GetVelocityFromRigidbodyMovement(Rigidbody interactiveRigidbody, Vector3 atPoint, float deltaTime)
        {
            if (deltaTime > 0f)
            {
                Vector3 effectiveMoverVelocity = interactiveRigidbody.velocity;

                if (interactiveRigidbody.angularVelocity != Vector3.zero)
                {
                    Vector3 centerOfRotation = interactiveRigidbody.position + interactiveRigidbody.centerOfMass;

                    Vector3 centerOfRotationToPoint = atPoint - centerOfRotation;
                    Quaternion rotationFromInteractiveRigidbody = Quaternion.Euler(Mathf.Rad2Deg * interactiveRigidbody.angularVelocity * deltaTime);
                    Vector3 finalPointPosition = centerOfRotation + (rotationFromInteractiveRigidbody * centerOfRotationToPoint);
                    effectiveMoverVelocity += (finalPointPosition - atPoint) / deltaTime;
                }
                return effectiveMoverVelocity;
            }
            else return Vector3.zero;
        }
        private Rigidbody GetInteractiveRigidbody(Collider onCollider) => onCollider.attachedRigidbody;
        public Vector3 GetVelocityForMovePosition(Vector3 fromPosition, Vector3 toPosition, float deltaTime)
        {
            if (deltaTime > 0) return (toPosition - fromPosition) / deltaTime;
            else return Vector3.zero;
        }
        private void RestrictVectorToPlane(ref Vector3 vector, Vector3 toPlane)
        {
            if (vector.x > 0 != toPlane.x > 0) vector.x = 0;
            if (vector.y > 0 != toPlane.y > 0) vector.y = 0;
            if (vector.z > 0 != toPlane.z > 0) vector.z = 0;
        }
        public int CharacterCollisionsOverlap(Vector3 atPosition, Quaternion atRotation, Collider[] overlappedColliders, float radiusInflate = 0f)
        {
            int nbHits = 0;
            int nbUnfilteredHits = Physics.OverlapCapsuleNonAlloc(atPosition + (atRotation * CharacterTransformToCapsuleBottomHemi),
                        atPosition + (atRotation * CharacterTransformToCapsuleTopHemi), capsuleCollider.radius + radiusInflate,
                        overlappedColliders, CollidableLayers, QueryTriggerInteraction.Ignore);
            // Filter out invalid colliders
            nbHits = nbUnfilteredHits;
            for (int i = nbUnfilteredHits - 1; i >= 0; i--)
            {
                if (!CheckIfColliderValidForCollisions(overlappedColliders[i]))
                {
                    nbHits--;
                    if (i < nbHits)
                    {
                        overlappedColliders[i] = overlappedColliders[nbHits];
                    }
                }
            }
            return nbHits;
        }
        public int CharacterOverlap(Vector3 atPosition, Quaternion atRotation, Collider[] overlappedColliders, LayerMask layers, QueryTriggerInteraction triggerInteraction, float radiusInflate = 0f)
        {
            int nbHits = 0;
            int nbUnfilteredHits = Physics.OverlapCapsuleNonAlloc(atPosition + (atRotation * CharacterTransformToCapsuleBottomHemi), atPosition + (atRotation * CharacterTransformToCapsuleTopHemi),
                        capsuleCollider.radius + radiusInflate, overlappedColliders, layers, triggerInteraction);

            // Filter out the character capsule itself
            nbHits = nbUnfilteredHits;
            for (int i = nbUnfilteredHits - 1; i >= 0; i--)
            {
                if (overlappedColliders[i] == capsuleCollider)
                {
                    nbHits--;
                    if (i < nbHits)
                    {
                        overlappedColliders[i] = overlappedColliders[nbHits];
                    }
                }
            }
            return nbHits;
        }
        public int CharacterCollisionsSweep(Vector3 position, Quaternion rotation, Vector3 direction, float distance, out RaycastHit closestHit, RaycastHit[] hits, float radiusInflate = 0f)
        {
            direction.Normalize();

            // Capsule cast
            int nbHits = 0;
            int nbUnfilteredHits = Physics.CapsuleCastNonAlloc(
                    position + (rotation * CharacterTransformToCapsuleBottomHemi) - (direction * SweepProbingBackstepDistance),
                    position + (rotation * CharacterTransformToCapsuleTopHemi) - (direction * SweepProbingBackstepDistance),
                    capsuleCollider.radius + radiusInflate,
                    direction,
                    hits,
                    distance + SweepProbingBackstepDistance,
                    CollidableLayers,
                    QueryTriggerInteraction.Ignore);

            // Hits filter
            closestHit = new RaycastHit();
            float closestDistance = Mathf.Infinity;
            nbHits = nbUnfilteredHits;
            for (int i = nbUnfilteredHits - 1; i >= 0; i--)
            {
                hits[i].distance -= SweepProbingBackstepDistance;

                // Filter out the invalid hits
                if (hits[i].distance <= 0f ||
                    !CheckIfColliderValidForCollisions(hits[i].collider))
                {
                    nbHits--;
                    if (i < nbHits)
                    {
                        hits[i] = hits[nbHits];
                    }
                }
                else
                {
                    // Remember closest valid hit
                    if (hits[i].distance < closestDistance)
                    {
                        closestHit = hits[i];
                        closestDistance = hits[i].distance;
                    }
                }
            }

            return nbHits;
        }
        public int CharacterSweep(Vector3 position, Quaternion rotation, Vector3 direction, float distance, out RaycastHit closestHit, RaycastHit[] hits, LayerMask layers, QueryTriggerInteraction triggerInteraction, float radiusInflate = 0f)
        {
            direction.Normalize();
            closestHit = new RaycastHit();

            // Capsule cast
            int nbHits = 0;
            int nbUnfilteredHits = Physics.CapsuleCastNonAlloc(
                position + (rotation * CharacterTransformToCapsuleBottomHemi),
                position + (rotation * CharacterTransformToCapsuleTopHemi),
                capsuleCollider.radius + radiusInflate,
                direction,
                hits,
                distance,
                layers,
                triggerInteraction);

            // Hits filter
            float closestDistance = Mathf.Infinity;
            nbHits = nbUnfilteredHits;
            for (int i = nbUnfilteredHits - 1; i >= 0; i--)
            {
                // Filter out the character capsule
                if (hits[i].distance <= 0f || hits[i].collider == capsuleCollider)
                {
                    nbHits--;
                    if (i < nbHits)
                    {
                        hits[i] = hits[nbHits];
                    }
                }
                else
                {
                    // Remember closest valid hit
                    if (hits[i].distance < closestDistance)
                    {
                        closestHit = hits[i];
                        closestDistance = hits[i].distance;
                    }
                }
            }

            return nbHits;
        }

        private bool CharacterGroundSweep(Vector3 position, Quaternion rotation, Vector3 direction, float distance, out RaycastHit closestHit)
        {
            direction.Normalize();
            closestHit = new RaycastHit();

            // Capsule cast
            int nbUnfilteredHits = Physics.CapsuleCastNonAlloc(
                position + (rotation * CharacterTransformToCapsuleBottomHemi) - (direction * GroundProbingBackstepDistance),
                position + (rotation * CharacterTransformToCapsuleTopHemi) - (direction * GroundProbingBackstepDistance),
                capsuleCollider.radius,
                direction,
                _internalCharacterHits,
                distance + GroundProbingBackstepDistance,
                CollidableLayers,
                QueryTriggerInteraction.Ignore);

            // Hits filter
            bool foundValidHit = false;
            float closestDistance = Mathf.Infinity;
            for (int i = 0; i < nbUnfilteredHits; i++)
            {
                // Find the closest valid hit
                if (_internalCharacterHits[i].distance > 0f && CheckIfColliderValidForCollisions(_internalCharacterHits[i].collider))
                {
                    if (_internalCharacterHits[i].distance < closestDistance)
                    {
                        closestHit = _internalCharacterHits[i];
                        closestHit.distance -= GroundProbingBackstepDistance;
                        closestDistance = _internalCharacterHits[i].distance;

                        foundValidHit = true;
                    }
                }
            }

            return foundValidHit;
        }
        public int CharacterCollisionsRaycast(Vector3 position, Vector3 direction, float distance, out RaycastHit closestHit, RaycastHit[] hits)
        {
            direction.Normalize();

            // Raycast
            int nbHits = 0;
            int nbUnfilteredHits = Physics.RaycastNonAlloc(
                position,
                direction,
                hits,
                distance,
                CollidableLayers,
                QueryTriggerInteraction.Ignore);

            // Hits filter
            closestHit = new RaycastHit();
            float closestDistance = Mathf.Infinity;
            nbHits = nbUnfilteredHits;
            for (int i = nbUnfilteredHits - 1; i >= 0; i--)
            {
                // Filter out the invalid hits
                if (hits[i].distance <= 0f ||
                    !CheckIfColliderValidForCollisions(hits[i].collider))
                {
                    nbHits--;
                    if (i < nbHits)
                    {
                        hits[i] = hits[nbHits];
                    }
                }
                else
                {
                    // Remember closest valid hit
                    if (hits[i].distance < closestDistance)
                    {
                        closestHit = hits[i];
                        closestDistance = hits[i].distance;
                    }
                }
            }

            return nbHits;
        }

        public virtual void HandleMovementProjection(ref Vector3 movement, Vector3 obstructionNormal, bool stableOnHit)
        {
            if (GroundingStatus.IsStableOnGround && !MustUnground)
            {
                if (stableOnHit) movement = GetDirectionTangentToSurface(movement, obstructionNormal) * movement.magnitude;
                else
                {
                    Vector3 obstructionRightAlongGround = Vector3.Cross(obstructionNormal, GroundingStatus.GroundNormal).normalized;
                    Vector3 obstructionUpAlongGround = Vector3.Cross(obstructionRightAlongGround, obstructionNormal).normalized;
                    movement = GetDirectionTangentToSurface(movement, obstructionUpAlongGround) * movement.magnitude;
                    movement = Vector3.ProjectOnPlane(movement, obstructionNormal);
                }
            }
            else
            {
                if (stableOnHit)
                {
                    movement = Vector3.ProjectOnPlane(movement, CharacterUp);
                    movement = GetDirectionTangentToSurface(movement, obstructionNormal) * movement.magnitude;
                }
                else movement = Vector3.ProjectOnPlane(movement, obstructionNormal);
            }
        }
        public virtual void HandleSimulatedRigidbodyInteraction(ref Vector3 processedVelocity, RigidbodyProjectionHit hit, float deltaTime)
        {
            float simulatedCharacterMass = 0.2f;

            if (simulatedCharacterMass > 0f && !hit.StableOnHit && !hit.Rigidbody.isKinematic)
            {
                float massRatio = simulatedCharacterMass / hit.Rigidbody.mass;
                Vector3 effectiveHitRigidbodyVelocity = GetVelocityFromRigidbodyMovement(hit.Rigidbody, hit.HitPoint, deltaTime);
                Vector3 relativeVelocity = Vector3.Project(hit.HitVelocity, hit.EffectiveHitNormal) - effectiveHitRigidbodyVelocity;
                hit.Rigidbody.AddForceAtPosition(massRatio * relativeVelocity, hit.HitPoint, ForceMode.VelocityChange);
            }

            if (!hit.StableOnHit)
            {
                Vector3 effectiveRigidbodyVelocity = GetVelocityFromRigidbodyMovement(hit.Rigidbody, hit.HitPoint, deltaTime);
                Vector3 projRigidbodyVelocity = Vector3.Project(effectiveRigidbodyVelocity, hit.EffectiveHitNormal);
                Vector3 projCharacterVelocity = Vector3.Project(processedVelocity, hit.EffectiveHitNormal);
                processedVelocity += projRigidbodyVelocity - projCharacterVelocity;
            }
        }
        public virtual void UpdateVelocity(ref Vector3 baseVelocity, float deltaTime) { }
        public virtual void OnDiscreteCollisionDetected(Collider collider) { }
        public virtual void AfterCharacterUpdate(float deltaTime) { }
        public virtual void UpdateRotation(ref Quaternion internalTransientRotation, float deltaTime) { }
        public virtual void PostGroundingUpdate(float deltaTime)
        {
            if (GroundingStatus.IsStableOnGround && !LastGroundingStatus.IsStableOnGround) OnLanded();
            else if (!GroundingStatus.IsStableOnGround && LastGroundingStatus.IsStableOnGround) OnLeaveStableGround();
        }
        public virtual void OnLeaveStableGround() { }
        public virtual void OnLanded() { }
        public virtual void BeforeCharacterUpdate(float deltaTime) { }
        public virtual void OnGroundHit(Collider collider, Vector3 normal, Vector3 point, ref HitStabilityReport groundHitStabilityReport) { }
        public virtual void OnMovementHit(Collider collider, Vector3 normal, Vector3 point, ref HitStabilityReport moveHitStabilityReport) { }
        public virtual bool IsColliderValidForCollisionsCustom(Collider coll) => false;
        public virtual void ProcessHitStabilityReport(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, Vector3 atCharacterPosition, Quaternion atCharacterRotation, ref HitStabilityReport stabilityReport) { }
    }
}