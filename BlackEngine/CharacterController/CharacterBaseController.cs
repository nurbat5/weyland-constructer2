﻿using UnityEngine;

namespace BlackEngine.CharacterController
{
    public class CharacterBaseController : CharacterMotor
    {
        protected CharacterState CurrentCharacterState { get; private set; }
        public virtual void Start() => TransitionToState(CharacterState.Default);
        public void TransitionToState(CharacterState newState)
        {
            CharacterState tmpInitialState = CurrentCharacterState;
            OnStateExit(tmpInitialState, newState);
            CurrentCharacterState = newState;
            OnStateEnter(newState, tmpInitialState);
        }
        public virtual void OnStateEnter(CharacterState state, CharacterState fromState) { }
        public virtual void OnStateExit(CharacterState state, CharacterState toState) { }
    }
}